package com.bai.insightapps.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bai.insightapps.R;
import com.bai.insightapps.databinding.RowItemInsightBinding;
import com.bai.insightapps.interfaces.SetOnItemClick;
import com.bai.insightapps.model.Info;
import com.bai.insightapps.utilities.Constants;
import com.bai.insightapps.utilities.Utils;

import java.text.ParseException;
import java.util.ArrayList;

public class InsightAdapter extends RecyclerView.Adapter<InsightAdapter.InsightViewHolder> {

    SetOnItemClick<Info> onItemClick;
    ArrayList<Info> listInsight;
    Activity activity;

    public InsightAdapter(ArrayList<Info> listInsight, SetOnItemClick<Info> onItemClick
            , Activity activity) {
        this.onItemClick = onItemClick;
        this.listInsight = listInsight;
        this.activity = activity;
    }

    @NonNull
    @Override
    public InsightViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowItemInsightBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext())
                , R.layout.row_item_insight, parent, false);
        return new InsightViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull InsightViewHolder holder, int position) {
        holder.bindItemData(listInsight.get(position));
    }

    @Override
    public int getItemCount() {
        return listInsight.size();
    }

    protected class InsightViewHolder extends RecyclerView.ViewHolder {
        RowItemInsightBinding binding;

        public InsightViewHolder(RowItemInsightBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        public void bindItemData(Info info) {
            try {
                String date = Utils.getInsightCommandDate(info.getMsgDate(), Constants.MONTH_YEAR_FORMATTER);
                binding.tvInsightDate.setText(date);
                binding.tvInsightTitle.setText(info.getTopic());
                binding.tvInsightDesc.setText(info.getMessage());
                if (info.getIsRead()) {
                    binding.idView.setVisibility(View.GONE);
                } else {
                    binding.idView.setVisibility(View.VISIBLE);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

            binding.getRoot().setOnClickListener(view -> {
                onItemClick.onItemClick(getAdapterPosition(), listInsight.get(getAdapterPosition()));
            });

        }
    }
}
