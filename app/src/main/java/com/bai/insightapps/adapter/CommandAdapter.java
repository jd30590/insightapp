package com.bai.insightapps.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bai.insightapps.R;
import com.bai.insightapps.databinding.RowItemCommandBinding;
import com.bai.insightapps.databinding.RowItemInsightBinding;
import com.bai.insightapps.interfaces.SetOnItemClick;
import com.bai.insightapps.model.Info;
import com.bai.insightapps.utilities.Constants;
import com.bai.insightapps.utilities.Utils;

import java.text.ParseException;
import java.util.ArrayList;

public class CommandAdapter extends RecyclerView.Adapter<CommandAdapter.CommandViewHolder> {

    SetOnItemClick<Info> onItemClick;
    ArrayList<Info> listCommand;

    public CommandAdapter(ArrayList<Info> listCommand, SetOnItemClick<Info> onItemClick) {
        this.onItemClick = onItemClick;
        this.listCommand = listCommand;
    }

    @NonNull
    @Override
    public CommandViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowItemCommandBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext())
                , R.layout.row_item_command, parent, false);
        return new CommandViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CommandViewHolder holder, int position) {
        holder.bindItemData(listCommand.get(position));
    }

    @Override
    public int getItemCount() {
        return listCommand.size();
    }

    protected class CommandViewHolder extends RecyclerView.ViewHolder {
        RowItemCommandBinding binding;

        public CommandViewHolder(RowItemCommandBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bindItemData(Info info) {
            if (info.getIsRead()) {
                binding.idView.setVisibility(View.GONE);
            } else {
                binding.idView.setVisibility(View.VISIBLE);
            }
            binding.tvCommandType.setText(info.getBroadcastMode());
            binding.tvCommandRole.setText(info.getSender());
            try {
                binding.tvCommandDate.setText(Utils.getInsightCommandDate(info.getMsgDate(), Constants.MONTH_YEAR_FORMATTER));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            binding.tvCommadDesc.setText(info.getMessage());
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick.onItemClick(getAdapterPosition(), listCommand.get(getAdapterPosition()));
                }
            });
        }
    }
}
