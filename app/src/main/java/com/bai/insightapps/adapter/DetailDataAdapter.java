package com.bai.insightapps.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bai.insightapps.R;
import com.bai.insightapps.databinding.RowItemDetailBackgroundBinding;
import com.bai.insightapps.databinding.RowItemDetailBinding;
import com.bai.insightapps.model.data.DataDetail;

import java.util.ArrayList;

public class DetailDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    ArrayList<DataDetail> dataDetails;
    static final int CONST_BACKGROUND_DATA = 2, CONST_DATA = 1;

    public DetailDataAdapter(Context context, ArrayList<DataDetail> dataDetails) {
        this.context = context;
        this.dataDetails = dataDetails;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == CONST_DATA) {
            RowItemDetailBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext())
                    , R.layout.row_item_detail
                    , parent
                    , false
            );
            return new DetailDataViewHolder(binding);
        } else {
            RowItemDetailBackgroundBinding binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext())
                    , R.layout.row_item_detail_background
                    , parent
                    , false
            );
            return new DetailDataViewHolderBackground(binding);

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case CONST_DATA:
                DetailDataViewHolder dataViewHolder = (DetailDataViewHolder) holder;
                dataViewHolder.bind(dataDetails.get(position));
                break;

            case CONST_BACKGROUND_DATA:
                DetailDataViewHolderBackground holderBackground = (DetailDataViewHolderBackground) holder;
                holderBackground.bind(dataDetails.get(position));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return CONST_DATA;
        } else {
            return CONST_BACKGROUND_DATA;
        }

    }

    @Override
    public int getItemCount() {
        return dataDetails.size();
    }

    class DetailDataViewHolder extends RecyclerView.ViewHolder {
        RowItemDetailBinding binding;

        public DetailDataViewHolder(RowItemDetailBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(DataDetail detail) {
            binding.tvValCat.setText(detail.getCategori());
            binding.tvValName.setText(detail.getName());
            binding.tvValValue.setText(detail.getValue());
        }
    }

    class DetailDataViewHolderBackground extends RecyclerView.ViewHolder {
        RowItemDetailBackgroundBinding binding;

        public DetailDataViewHolderBackground(RowItemDetailBackgroundBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(DataDetail detail) {
            binding.tvLblCat.setText(detail.getCategori());
            binding.tvLblName.setText(detail.getName());
            binding.tvLblValue.setText(detail.getValue());
        }
    }

}
