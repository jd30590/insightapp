package com.bai.insightapps.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bai.insightapps.R;
import com.bai.insightapps.databinding.RowItemSliderBinding;
import com.bai.insightapps.interfaces.SetOnItemClick;
import com.bai.insightapps.model.SliderItem;
import com.bai.insightapps.utilities.SliderAdapterItem;

import java.util.ArrayList;

public class SliderItemAdapter extends RecyclerView.Adapter<SliderItemAdapter.ViewHolderSliderItem> {

    SetOnItemClick<SliderItem> clickItem;
    Activity activity;
    ArrayList<SliderItem> listItem = new ArrayList<>();
    int prvSelectedItem = -1;

    public SliderItemAdapter(SetOnItemClick<SliderItem> clickItem, Activity activity) {
        this.clickItem = clickItem;
        this.activity = activity;
        listItem = SliderAdapterItem.getSliderItem(activity);
    }

    @NonNull
    @Override
    public ViewHolderSliderItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowItemSliderBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.row_item_slider
                , parent, false);
        return new ViewHolderSliderItem(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderSliderItem holder, int position) {
        holder.bind(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public class ViewHolderSliderItem extends RecyclerView.ViewHolder {
        RowItemSliderBinding binding;

        public ViewHolderSliderItem(RowItemSliderBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        public void bind(SliderItem item) {
            binding.ivSliderItem.setImageResource(item.getImageRes());
            binding.tvSliderTitle.setText(item.getTitle());
            if (item.getSelected() == 1) {
                binding.ivSliderItem.setColorFilter(ContextCompat.getColor(activity
                                , R.color.color_item_selected)
                        , android.graphics.PorterDuff.Mode.MULTIPLY);
                binding.llSliderItem.setBackground(activity.getResources()
                        .getDrawable(R.drawable.drawable_underline_selected));

            } else {
                binding.ivSliderItem.setColorFilter(ContextCompat
                                .getColor(activity, R.color.colorLblEnterLogin)
                        , android.graphics.PorterDuff.Mode.MULTIPLY);
                binding.llSliderItem.setBackground(activity.getResources()
                        .getDrawable(R.drawable.drawable_underline));
            }

            binding.llSliderItem.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("NotifyDataSetChanged")
                @Override
                public void onClick(View view) {
                    clickItem.onItemClick(getAdapterPosition()
                            , listItem.get(getAdapterPosition()));
                    for (int cnt = 0; cnt < listItem.size()-1; cnt++) {
                        if (cnt == getAdapterPosition()) {
                            listItem.get(cnt).setSelected(1);
                        } else {
                            listItem.get(cnt).setSelected(0);
                        }
                    }
                    notifyDataSetChanged();
                }
            });
        }
    }
}
