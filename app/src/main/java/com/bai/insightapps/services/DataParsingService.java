package com.bai.insightapps.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

import com.bai.insightapps.utilities.InsightsApp;

public class DataParsingService extends IntentService {
    private static final String TAG = "DataParsingService";

    public DataParsingService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        InsightsApp.getOperation();
    }
}
