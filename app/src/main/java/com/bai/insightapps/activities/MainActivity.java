package com.bai.insightapps.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.core.view.WindowCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.bai.insightapps.R;
import com.bai.insightapps.adapter.SliderItemAdapter;
import com.bai.insightapps.bottomsheet.FilterBottomSheetView;
import com.bai.insightapps.bottomsheet.SortBottomSheetView;
import com.bai.insightapps.databinding.ActivityMainBinding;
import com.bai.insightapps.databinding.LayoutFilterBottomSheetBinding;
import com.bai.insightapps.fragment.InsightCommandFragment;
import com.bai.insightapps.fragment.InsightFragment;
import com.bai.insightapps.interfaces.SetOnItemClick;
import com.bai.insightapps.model.SliderItem;
import com.bai.insightapps.utilities.ActivityUtils;
import com.bai.insightapps.utilities.Constants;
import com.bai.insightapps.utilities.FileOperation;
import com.bai.insightapps.utilities.PrefManager;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;

import java.util.Date;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    SortBottomSheetView sortBottomSheetView;
    FilterBottomSheetView filterBottomSheetView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        transperentStatusBar();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        sortBottomSheetView = new SortBottomSheetView(MainActivity.this
                , setOnButtonClick);
        filterBottomSheetView = new FilterBottomSheetView(this, setOnFilterButtonClick);
        setSliderAdapter();
        handleClickEvenComponent();
        openFragment(new InsightCommandFragment());
    }


    void transperentStatusBar() {
        Window w = getWindow();
        w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        w.setStatusBarColor(Color.TRANSPARENT);
        w.setNavigationBarColor(Color.TRANSPARENT);
        w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    }

    void setSliderAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.rvSliderItem.setLayoutManager(layoutManager);
        SliderItemAdapter itemAdapter = new SliderItemAdapter(onItemClick, this);
        binding.rvSliderItem.setAdapter(itemAdapter);
    }

    private void openFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragTran = fragmentManager.beginTransaction();
        fragTran.replace(R.id.container, fragment);
        fragTran.commit();
    }

    private void handleClickEvenComponent() {
        binding.toolbar.ivDrawer.setOnClickListener(clickListener);
        binding.toolbar.ivSort.setOnClickListener(clickListener);
        binding.toolbar.ivFilter.setOnClickListener(clickListener);
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivDrawer:
                    binding.drawerLayout.openDrawer(GravityCompat.START);
                    break;
                case R.id.ivSort:
                    setArrayInBottomSheetView();
                    sortBottomSheetView.setFlag(getActiveFragment());
                    sortBottomSheetView.showBottomSheetSort();
                    break;
                case R.id.ivFilter:
                    showFilterBottomSheet();
                    break;
            }
        }
    };

    private int getActiveFragment() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentById(R.id.container);
        Fragment fragment1 = null;
        if (fragment instanceof InsightCommandFragment) {
            fragment1 = ((InsightCommandFragment) fragment).getActiveFragment();
        }
        if (fragment1 instanceof InsightFragment) {
            return 1;
        } else {
            return 2;
        }
    }

    SetOnItemClick<SliderItem> onItemClick = new SetOnItemClick<SliderItem>() {
        @Override
        public void onItemClick(int position, SliderItem Item) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
            switch (Item.getId()) {
                case Constants.SLIDER_HOME:
                    break;
                case Constants.SLIDER_PLAN:
                    break;
                case Constants.SLIDER_PROGRESS:
                    break;
                case Constants.SLIDER_SYNC:
                    break;
                case Constants.SLIDER_SETTING:
                    break;
                case Constants.SLIDER_LOGOUT:
                    showLogoutDialog();
                    break;
            }
        }
    };

    private void showLogoutDialog() {
        FancyAlertDialog.Builder
                .with(this)
                .setTitle(getResources().getString(R.string.lbl_slider_logout))
                .setMessage(getResources().getString(R.string.msgLogout))
                .setNegativeBtnText(getResources().getString(R.string.lblCancel))
                .setPositiveBtnText(getResources().getString(R.string.lblOK))
//                .setAnimation(Animation.POP)
                .isCancellable(false)
                .setBackgroundColor(getResources().getColor(R.color.primaryColor))
                .isCancellable(true)
                .setIcon(android.R.drawable.ic_dialog_alert, View.VISIBLE)
                .onPositiveClicked(dialog -> {
                    dialog.dismiss();
                    PrefManager.getInstance().remove(Constants.PREF_IS_LOGIN);
                    ActivityUtils.launchActivityWithClearBackStack(MainActivity.this, LoginActivity.class);

                })
                .onNegativeClicked(Dialog::dismiss)
                .build()
                .show();
    }

    private void showFilterBottomSheet() {
        sortBottomSheetView.setFlag(getActiveFragment());
        setArrayInBottomSheetView();
        filterBottomSheetView.showBottomSheetSort();
    }

    private void setArrayInBottomSheetView() {
        filterBottomSheetView.getArrayListFilter();
        sortBottomSheetView.getArrayList();
    }

    //Bottom Sheet sort view apply and clear button click listner.
    SortBottomSheetView.SetOnButtonClick setOnButtonClick = new SortBottomSheetView.SetOnButtonClick() {
        @Override
        public void setOnClearButtonClick() {
            finish();
        }

        @Override
        public void setOnApplyButtonClick() {
            finish();
        }
    };

    FilterBottomSheetView.SetOnButtonClick setOnFilterButtonClick = new FilterBottomSheetView.SetOnButtonClick() {
        @Override
        public void setOnClearFilterClick() {

        }

        @Override
        public void setOnApplyFilterClick() {

        }
    };


    Date dateFrom, dateTo;
    int dateFlag = 1, intSelTopicPos = 0, selReadStatusPos = 0;
    String selWord = "";

    public void setFilterSelectedData(Date dateFrom, Date dateTo,
                                      int dateFlag, int intSelTopicPos, int selReadStatusPos,
                                      String selWord) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.dateFlag = dateFlag;
        this.intSelTopicPos = intSelTopicPos;
        this.selReadStatusPos = selReadStatusPos;
        this.selWord = selWord;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public int getDateFlag() {
        return dateFlag;
    }

    public int getIntSelTopicPos() {
        return intSelTopicPos;
    }

    public int getSelReadStatusPos() {
        return selReadStatusPos;
    }

    public String getSelWord() {
        return selWord;
    }

    public void changeFilterIconColor(int clearFlag) {
        if (clearFlag == 1) {
            binding.toolbar.ivFilter.setImageResource(R.drawable.ic_filter);
        } else {
            binding.toolbar.ivFilter.setImageResource(R.drawable.ic_filter_selected);
        }
    }
}