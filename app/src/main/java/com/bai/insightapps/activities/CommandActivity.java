package com.bai.insightapps.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.bai.insightapps.R;
import com.bai.insightapps.databinding.ActivityCommandBinding;
import com.bai.insightapps.model.Info;
import com.bai.insightapps.utilities.ConstantBundleKey;
import com.bai.insightapps.utilities.Constants;
import com.bai.insightapps.utilities.Utils;

import java.text.ParseException;

public class CommandActivity extends AppCompatActivity {
    ActivityCommandBinding binding;
    Info info;
    int messageId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        transperentStatusBar();
        binding = DataBindingUtil.setContentView(this, R.layout.activity_command);
        getDataFromIntent();
        setToolBar();
        setClickEventOnView();
        displayData();
    }

    public void getDataFromIntent() {
        Bundle bundle = getIntent().getExtras();
        info = bundle != null ? bundle.containsKey(ConstantBundleKey.CONSTANT_DATA)
                ? (Info) bundle.getSerializable(ConstantBundleKey.CONSTANT_DATA) : null : null;
        messageId = bundle != null ? bundle.containsKey(ConstantBundleKey.MESSAGE_ID)
                ? bundle.getInt(ConstantBundleKey.MESSAGE_ID) : -1 : -1;
    }

    void transperentStatusBar() {
        Window w = getWindow();
        w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        w.setStatusBarColor(Color.TRANSPARENT);
        w.setNavigationBarColor(Color.TRANSPARENT);
        w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    }

    void setToolBar() {
        binding.toolbar.ivDrawer.setImageResource(R.drawable.ic_back_activity);
        binding.toolbar.ivFilter.setVisibility(View.GONE);
        binding.toolbar.ivSort.setVisibility(View.GONE);
    }

    void setClickEventOnView() {
        binding.btnLogin.setOnClickListener(clickOnView);
        binding.toolbar.ivDrawer.setOnClickListener(clickOnView);
    }

    private void displayData() {
        if (info != null) {
            binding.tvCommandDesc.setText(info.getMessage());
            binding.tvRole.setText(info.getSender());
            try {
                binding.tvDate.setText(Utils.getInsightCommandDate(info.getMsgDate(), Constants.MONTH_YEAR_FORMATTER));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            Utils.showToast(getApplicationContext(),R.string.noValueFound);
            finish();
        }
    }

    View.OnClickListener clickOnView = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivDrawer:
                    finish();
                    break;
                case R.id.btnLogin:
                    finish();
                    break;
            }
        }
    };
}