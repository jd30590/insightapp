package com.bai.insightapps.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.SyncResult;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.bai.insightapps.R;
import com.bai.insightapps.backgroundprocess.ReadCharData;
import com.bai.insightapps.databinding.ActivityInsightBinding;
import com.bai.insightapps.databinding.LayoutProgressChartBinding;
import com.bai.insightapps.fragment.DetailDialogFragment;
import com.bai.insightapps.model.Info;
import com.bai.insightapps.model.chart.ChartDatum;
import com.bai.insightapps.model.chart.ChartDetail;
import com.bai.insightapps.utilities.ConstantBundleKey;
import com.bai.insightapps.utilities.Constants;
import com.bai.insightapps.utilities.LoadingIndicatorDialog;
import com.bai.insightapps.utilities.Utils;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

public class InsightActivity extends AppCompatActivity {
    ActivityInsightBinding binding;
    Info info;
    int messageId = -1;
    Bundle bundle;
    ReadCharData readCharData;
    LoadingIndicatorDialog indicatorDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_insight);
        indicatorDialog = new LoadingIndicatorDialog(this);
        getDataFromIntent();

        transperentStatusBar();
        setUpToolBar();
        displayData();
        getChartData();
        mangeClickEvent();
    }

    public void getDataFromIntent() {
        bundle = getIntent().getExtras();
        info = bundle != null ? bundle.containsKey(ConstantBundleKey.CONSTANT_DATA)
                ? (Info) bundle.getSerializable(ConstantBundleKey.CONSTANT_DATA) : null : null;
        messageId = bundle != null ? bundle.containsKey(ConstantBundleKey.MESSAGE_ID)
                ? bundle.getInt(ConstantBundleKey.MESSAGE_ID) : -1 : -1;
    }

    private void getChartData() {
        readCharData = new ReadCharData(messageId, new ReadCharData.Callback() {

            @Override
            public void onChartData(BarData barData
                    , LineData lineData
                    , ArrayList<String> listDate
                    , ArrayList<ChartDatum> listProgressData
                    , ArrayList<String> listCatgory
                    , ArrayList<String> listLineChartDate
                    , ArrayList<String> listLineChartCat) {
                showBarChart(barData, listDate, listCatgory);
                setupLineChart(lineData, listLineChartDate, listLineChartCat);
                setupProgressChart(listProgressData);
                setChartTitle();
                //indicatorDialog.dismiss();
            }
        }, InsightActivity.this);
       // indicatorDialog.show();
        readCharData.doBackgroundProcess();
    }

    private void setUpToolBar() {
        binding.toolbar.ivSort.setVisibility(View.GONE);
        binding.toolbar.ivFilter.setVisibility(View.GONE);
        binding.toolbar.ivDrawer.setImageResource(R.drawable.ic_back_activity);
    }

    private void mangeClickEvent() {
        binding.tvDetail.setOnClickListener(clickListener);
        binding.toolbar.ivDrawer.setOnClickListener(clickListener);
    }

    void transperentStatusBar() {
        Window w = getWindow();
        w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        w.setStatusBarColor(Color.TRANSPARENT);
        w.setNavigationBarColor(Color.TRANSPARENT);
        w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    }

    private void displayData() {
        binding.tvCommandDesc.setText(info.getMessage());
        String date = "";
        try {
            date = Utils.getInsightCommandDate(info.getMsgDate(), Constants.MONTH_YEAR_FORMATTER);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        binding.tvDate.setText(date);
        date = getResources().getString(R.string.lblLastDate) + " " +
                date;
        binding.tvInsightDate.setText(date);
        displayPeriodTitle();
    }

    private void displayPeriodTitle() {
        String title = getResources().getString(R.string.lblDailyInsight);
        switch (info.getPeriod()) {
            case ConstantBundleKey.DAILY:
                title = getResources().getString(R.string.lblDailyInsight);
                break;
            case ConstantBundleKey.WEEKLY:
                title = getResources().getString(R.string.lblWeaklyInsight);
                break;
            case ConstantBundleKey.MONTHLY:
                title = getResources().getString(R.string.lblMonthlyInsight);
                break;
        }
        binding.tvLblInsight.setText(title);
    }

    View.OnClickListener clickListener = view -> {
        switch (view.getId()) {
            case R.id.tvDetail:
                showDetailFragment();
                break;
            case R.id.ivDrawer:
                finish();
                break;
        }
    };

    private void showDetailFragment() {
        DetailDialogFragment dialogFragment = DetailDialogFragment.getInstance(messageId);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        dialogFragment.show(ft, "dialog");
    }
    /********* Setup chart Title**********/
    private void setChartTitle(){
        binding.lblProgTitle.setText(readCharData.getProgChartTitle());
        binding.lblLineChart.setText(readCharData.getLineChartTitle());
        binding.lblBarChart.setText(readCharData.getBarChartTitle());
    }

    /***************** progress bar chart*************/
    public void setupProgressChart(ArrayList<ChartDatum> listProgressData) {
        LayoutInflater layoutInfralte = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int cntPrg=0;
        for (ChartDatum datum : listProgressData) {
            LayoutProgressChartBinding bindingChart = DataBindingUtil.inflate(layoutInfralte, R.layout.layout_progress_chart, null, false);
            bindingChart.lblProgressChart.setText(datum.getSubCategory());
            bindingChart.progressBar.setProgressPercentage(datum.getPercentage(), true);
            LinearLayout.LayoutParams viewMargin = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            viewMargin.setMargins(0, 30, 0, 0);
            bindingChart.getRoot().setLayoutParams(viewMargin);
            binding.llProgressChart.addView(bindingChart.getRoot(), viewMargin);
            cntPrg=cntPrg+1;
        }
    }

    /********************Bar Chart data *********************/
    int valueCount = 1;

    //https://medium.com/@clyeung0714/using-mpandroidchart-for-android-application-barchart-540a55b4b9ef
    private void showBarChart(BarData barData, ArrayList<String> listDates, ArrayList<String> listCatgory) {
        binding.barChart.setData(barData);

        binding.barChart.getXAxis().setGranularity(1.0f);
        binding.barChart.getXAxis().setGranularityEnabled(true);
        binding.barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(listDates));
        binding.barChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        binding.barChart.setExtraOffsets(5f, 5f, 5f, 15f);
        binding.barChart.setVisibleXRangeMaximum(3);
        binding.barChart.getDescription().setEnabled(false);
        binding.barChart.getData().setBarWidth(0.5f);
        Legend legend = binding.barChart.getLegend();
        legend.setEnabled(true);
        legend.setTextSize(15f);

        LegendEntry legendEntry[] = legend.getEntries();
        for (int cnt = 0; cnt < listCatgory.size(); cnt++) {
            legendEntry[cnt].label = listCatgory.get(cnt);
        }
        legend.setCustom(legendEntry);
        binding.barChart.getLegend().setXEntrySpace(50f);
        binding.barChart.getLegend().setYEntrySpace(0f);
        binding.barChart.getLegend().setYOffset(10f);
        binding.barChart.getLegend().setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);

        binding.barChart.setExtraOffsets(5f, 5f, 5f, 10f);
        binding.barChart.getLegend().setWordWrapEnabled(true);
        binding.barChart.setFocusable(false);
        binding.barChart.setVisibleXRangeMaximum(3);

        binding.barChart.invalidate();
    }


    /*********** Line Chart *******************/

    private void setupLineChart(LineData lineData, ArrayList<String> listDate
            , ArrayList<String> listCatgory) {
//        https://github.com/PhilJay/MPAndroidChart/issues/2790

        // - X Axis
        XAxis xAxis = binding.lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextColor(ColorTemplate.getHoloBlue());
        xAxis.setEnabled(true);
        xAxis.disableGridDashedLine();
        xAxis.setGranularity(1f); // only intervals of 1 day
        xAxis.setDrawGridLines(true);
        xAxis.setAvoidFirstLastClipping(true);


        binding.lineChart.setData(lineData);

        Legend legend = binding.lineChart.getLegend();
        legend.setEnabled(true);
        legend.setTextSize(15f);

        LegendEntry legendEntry[] = legend.getEntries();
        for (int cnt = 0; cnt < listCatgory.size(); cnt++) {
            legendEntry[cnt].label = listCatgory.get(cnt);
        }
        legend.setCustom(legendEntry);

        binding.lineChart.getLegend().setXEntrySpace(60f);
        binding.lineChart.getLegend().setYEntrySpace(0f);
        binding.lineChart.getLegend().setYOffset(10f);
        binding.lineChart.getLegend().setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);

        binding.lineChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(listDate));
        binding.lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        binding.lineChart.setExtraOffsets(5f, 5f, 5f, 15f);
        binding.lineChart.setVisibleXRangeMaximum(3);

        binding.lineChart.getDescription().setEnabled(false);
        binding.lineChart.setPinchZoom(false);
        binding.lineChart.setDoubleTapToZoomEnabled(false);
        binding.lineChart.invalidate();
    }
}