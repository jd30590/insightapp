package com.bai.insightapps.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import android.database.DatabaseUtils;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;

import com.bai.insightapps.R;
import com.bai.insightapps.databinding.ActivityLoginBinding;
import com.bai.insightapps.utilities.ActivityUtils;
import com.bai.insightapps.utilities.Constants;
import com.bai.insightapps.utilities.PrefManager;
import com.bai.insightapps.utilities.Utils;
import com.bai.insightapps.utilities.Validation;

public class LoginActivity extends AppCompatActivity {
    ActivityLoginBinding binding;
    int showPassword = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.ivShowPassword.setOnClickListener(clickListener);
        binding.btnLogin.setOnClickListener(clickOnLoginButton);
        binding.ivShowPassword.setImageResource(R.drawable.ic_hidepassword);
        binding.edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            if (showPassword == 0) {
                binding.ivShowPassword.setImageResource(R.drawable.ic_showpassword);
                showPassword = 1;
                binding.edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            } else {
                binding.edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                binding.ivShowPassword.setImageResource(R.drawable.ic_hidepassword);
                showPassword = 0;
            }

            binding.edtPassword.setSelection(binding.edtPassword.getText().length());
        }
    };

    View.OnClickListener clickOnLoginButton = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // openActivity();
            matchValidation();
        }
    };

    private void matchValidation() {
        String userName = binding.edtEmail.getText().toString().trim();
        String password = binding.edtPassword.getText().toString().trim();
        if (userName.isEmpty()) {
            Utils.showToast(this, R.string.msgEnterUserName);
            return;
        }

        if (password.isEmpty()) {
            Utils.showToast(this, R.string.msgEnterPasswrod);
            return;
        }
        if (password.length() < 8) {
            Utils.showToast(this, R.string.msgPswLenthErr);
            return;
        }
        if (!Validation.isValidPassword(password)) {
            Utils.showToast(this, R.string.msgValidationPsw);
            return;
        }
        openActivity();
    }

    private void openActivity() {
        PrefManager.getInstance().save(Constants.PREF_IS_LOGIN, true);
        ActivityUtils.launchActivityWithClearBackStack(LoginActivity.this, MainActivity.class);
    }
}