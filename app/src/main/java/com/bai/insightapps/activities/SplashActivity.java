package com.bai.insightapps.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.bai.insightapps.R;
import com.bai.insightapps.services.DataParsingService;
import com.bai.insightapps.utilities.ActivityUtils;
import com.bai.insightapps.utilities.Constants;
import com.bai.insightapps.utilities.PrefManager;

public class SplashActivity extends AppCompatActivity {
    PrefManager pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        pref = PrefManager.getInstance();
        Intent intent = new Intent(this, DataParsingService.class);
        startService(intent);
        openMainOrLoginActivity();
    }

    private void openMainOrLoginActivity() {
        new Handler().postDelayed(() -> {
            boolean isLogin
                    = pref.getBoolean(Constants.PREF_IS_LOGIN, false);
            if (isLogin) {
                ActivityUtils.launchActivityWithClearBackStack(this, MainActivity.class);
            } else {
                ActivityUtils.launchActivityWithClearBackStack(this, LoginActivity.class);
            }
        }, 3000);
    }
}