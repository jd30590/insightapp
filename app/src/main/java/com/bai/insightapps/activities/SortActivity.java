package com.bai.insightapps.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Toast;

import com.bai.insightapps.R;
import com.bai.insightapps.databinding.ActivitySortBinding;
import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;

import java.util.ArrayList;

public class SortActivity extends AppCompatActivity {
    ActivitySortBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sort);
        transperentStatusBar();
        setUpToolBar();
        setTypeFaceSwitch();
        setSwitchEvent();
        setClickOnButton();

        initSpinner();
    }

    void transperentStatusBar() {
        Window w = getWindow();
        w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        w.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        w.setStatusBarColor(Color.TRANSPARENT);
        w.setNavigationBarColor(Color.TRANSPARENT);
        w.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
    }

    private void setUpToolBar() {
        binding.incToolBar.ivDrawer.setImageResource(R.drawable.ic_back_activity);
        binding.incToolBar.ivSort.setVisibility(View.GONE);
        binding.incToolBar.ivFilter.setVisibility(View.GONE);
        binding.incToolBar.ivDrawer.setOnClickListener(clickBackIcon);
    }

    private void setClickOnButton() {
        binding.tvApply.setOnClickListener(clickOnButton);
        binding.tvClear.setOnClickListener(clickOnButton);
    }

    private View.OnClickListener clickBackIcon = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    private View.OnClickListener clickOnButton = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tvClear:
                    break;
                case R.id.tvApply:
                    break;
            }
            finish();
        }
    };


    private void setTypeFaceSwitch() {
        Typeface interExtraBold = ResourcesCompat.getFont(SortActivity.this, R.font.inter_extrabold);
        binding.switch1.setTypeface(interExtraBold);
        binding.switch2.setTypeface(interExtraBold);
        binding.switch3.setTypeface(interExtraBold);
    }

    private void setSwitchEvent() {

        binding.switch1.setOnToggledListener(toggledListener);
        binding.switch2.setOnToggledListener(toggledListener);
        binding.switch3.setOnToggledListener(toggledListener);
    }

    OnToggledListener toggledListener = new OnToggledListener() {
        @Override
        public void onSwitched(ToggleableView toggleableView, boolean isOn) {
            if (isOn) {
                setSwitchColor((LabeledSwitch) toggleableView
                        , getResources().getColor(R.color.color_item_selected),
                        getResources().getColor(R.color.white));
            } else {
                setSwitchColor((LabeledSwitch) toggleableView
                        , getResources().getColor(R.color.white),
                        getResources().getColor(R.color.color_off_switch));
            }
            switch (toggleableView.getId()) {
                case R.id.switch1:
                    break;
                case R.id.switch2:
                    break;
                case R.id.switch3:
                    break;
            }
        }
    };

    private void setSwitchColor(LabeledSwitch lblSwitch, int colorOn, int colorOff) {
        lblSwitch.setColorOn(colorOn);
        lblSwitch.setColorOff(colorOff);
    }

    private void initSpinner() {

        ArrayList<String> sortLevel1 = new ArrayList<>();

        sortLevel1.add(getResources().getString(R.string.itemDate));
        sortLevel1.add(getResources().getString(R.string.hintTopic));
        sortLevel1.add(getResources().getString(R.string.hintReadStatus));

        binding.spinner1.setItem(sortLevel1);
        binding.spinner2.setItem(sortLevel1);
        binding.spinner3.setItem(sortLevel1);
        binding.spinner1.setSelection(0);
        binding.spinner2.setSelection(0);
        binding.spinner3.setSelection(0);

        binding.spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                //Toast.makeText(SortActivity.this, sortLevel1.get(position), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        binding.spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
               // Toast.makeText(SortActivity.this, sortLevel1.get(position), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        binding.spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
               // Toast.makeText(SortActivity.this, sortLevel1.get(position), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }
}