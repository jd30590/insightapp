package com.bai.insightapps.backgroundprocess;

import android.app.Activity;
import android.graphics.Color;
import android.os.Handler;
import android.util.Log;

import com.bai.insightapps.R;
import com.bai.insightapps.model.chart.ChartDatum;
import com.bai.insightapps.model.chart.ChartDetail;
import com.bai.insightapps.utilities.ConstantBundleKey;
import com.bai.insightapps.utilities.Constants;
import com.bai.insightapps.utilities.FileOperation;
import com.bai.insightapps.utilities.Utils;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ReadCharData {
    ExecutorService executors = Executors.newSingleThreadExecutor();
    Handler handler = new Handler();
    int messageId = -1;
    Activity activity;
    ChartDetail lineChartData;
    ChartDetail barCharData;
    ChartDetail processChart;
    ArrayList<ChartDatum> listProgressData = new ArrayList<>();

    LinkedHashMap<Date, ArrayList<Float>> mapBarChart = new LinkedHashMap<>();
    LinkedHashMap<String, String> mapCategories = new LinkedHashMap<>();
    LinkedHashMap<Date, String> mapDate = new LinkedHashMap<>();

    LinkedHashMap<String, ArrayList<Float>> mapLineChart = new LinkedHashMap<>();
    LinkedHashMap<String, String> mapLineCategories = new LinkedHashMap<>();
    LinkedHashMap<Date, String> mapLineDate = new LinkedHashMap<>();

    Callback callback;
    ArrayList<BarEntry> entries = new ArrayList<>();
    ArrayList<ArrayList<Entry>> lineChartValue = new ArrayList<>();
    LineData lineData;
    BarData barData;
    ArrayList<String> listDate = new ArrayList<>();
    ArrayList<String> listCategories = new ArrayList<>();
    ArrayList<String> listLineChartDate = new ArrayList<>();
    ArrayList<String> listLineChartCategories = new ArrayList<>();

    String barChartTitle, lineChartTitle, progChartTitle;

    public interface Callback {

        void onChartData(BarData barData
                , LineData lineData
                , ArrayList<String> listDate
                , ArrayList<ChartDatum> listProgressData
                , ArrayList<String> listCategories
                , ArrayList<String> listLineChartDate
                , ArrayList<String> listLineChartCat);
    }

    public ReadCharData(int messageId, Callback callback, Activity activity) {
        this.messageId = messageId;
        this.callback = callback;
        this.activity = activity;
    }

    public void doBackgroundProcess() {
        executors.execute(() -> {

            //Dfferentciate Bar, Line and progress Chart Data
            differeciateChartData();

            //Generate progress chart based on progress chart
            generateProgressChartData();

            //Manupulation Bar chart data
            manupulationOfBarChartData();
            //Generate categories
            generateCategories();

            //Generate dates
            generateDateData();

            //Generate bar chart
            generateBarChart();

            //Manupulation Line chart data
            manupulationOfLineChartData();

            //Generate line chart
            generateLineChart();

            //Generate category line chart
            generateLineChartCategories();

            //Generate line chart Date
            generateLineChartDate();

            handler.post(() -> {
                callback.onChartData(barData, lineData, listDate, listProgressDataTemp, listCategories
                        , listLineChartDate, listLineChartCategories);
            });
        });
    }

    //
    private void differeciateChartData() {
        for (ChartDetail chartDetail : FileOperation.getChartDetail().getChartDetail()) {
            if (chartDetail.getChartId() == 1) {
                lineChartData = chartDetail;
                lineChartTitle = chartDetail.getTitle();
            } else if (chartDetail.getChartId() == 2) {
                barCharData = chartDetail;
                barChartTitle = chartDetail.getTitle();
            } else if (chartDetail.getChartId() == 3) {
                processChart = chartDetail;
                progChartTitle = chartDetail.getTitle();
            }
        }
    }
    ArrayList<ChartDatum> listProgressDataTemp = new ArrayList<>();
    private void generateProgressChartData() {
        listProgressData = (ArrayList<ChartDatum>) processChart.getChartData();

        double total = 0;
        for (ChartDatum datum : listProgressData) {
            total = total + Double.parseDouble(datum.getValue());
        }
        int incrementCnt = 0;
        for (ChartDatum datum : listProgressData) {
            if (datum.getSubCategory().equalsIgnoreCase("Progress")) {

                long percentage = Math.round(100.0 *
                        Double.parseDouble(datum.getValue()) / total);
                datum.setPercentage((double) percentage);
                listProgressData.set(incrementCnt, datum);
                listProgressDataTemp.add(datum);
            }
            incrementCnt = incrementCnt + 1;
        }
    }

    //Create Hash map for Bar And chart
    private void manupulationOfBarChartData() {
        ArrayList<Float> chartDataList = null;
        ArrayList<Float> chartLineData = null;
        for (ChartDatum datum : barCharData.getChartData()) {
            Date msgDate = null;
            try {
                msgDate = FileOperation.stringToDate(datum.getDate(), Constants.CHART_DATE_FORMAT);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            datum.setDateForm(msgDate);
            if (mapBarChart.get(datum.getDateForm()) == null) {
                chartDataList = new ArrayList<>();
            }
            chartDataList.add(Float.parseFloat(datum.getValue()));
            mapBarChart.put(datum.getDateForm(), chartDataList);
            mapCategories.put(datum.getSubCategory(), datum.getSubCategory());
            try {
                mapDate.put(datum.getDateForm(), Utils.dateToString(datum.getDateForm(), "dd/MM"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        Log.e("sizeMap", String.valueOf(mapBarChart.size()));
        Log.e("sizeCat", String.valueOf(mapCategories.size()));
        Log.e("sizeDate", String.valueOf(mapBarChart.size()));
    }

    //generate Categories
    private void generateCategories() {
        //Categories
        for (Map.Entry<String, String> entry : mapCategories.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            listCategories.add(entry.getValue());
        }
    }

    private void generateDateData() {
        //String dates
        for (Map.Entry<Date, String> entry : mapDate.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            listDate.add(entry.getValue());
        }
    }

    //Create Hash map for line chart
    private void manupulationOfLineChartData() {
        ArrayList<Float> chartLineData = null;
        for (ChartDatum datum : lineChartData.getChartData()) {
            Date msgDate = null;
            try {
                msgDate = FileOperation.stringToDate(datum.getDate(), Constants.CHART_DATE_FORMAT);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            datum.setDateForm(msgDate);

            if (mapLineChart.get(datum.getSubCategory()) == null) {
                chartLineData = new ArrayList<>();
            } else {
                chartLineData = mapLineChart.get(datum.getSubCategory());

            }
            chartLineData.add(Float.parseFloat(datum.getValue()));
            mapLineChart.put(datum.getSubCategory(), chartLineData);

            mapLineCategories.put(datum.getSubCategory(), datum.getSubCategory());
            try {
                mapLineDate.put(datum.getDateForm(), Utils.dateToString(datum.getDateForm(), "dd/MM"));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        Log.e("sizeLineMap", String.valueOf(mapLineChart.size()));
        Log.e("sizeCat", String.valueOf(mapLineCategories.size()));
        Log.e("sizeDate", String.valueOf(mapLineDate.size()));
    }

    //generate Categories
    private void generateLineChartCategories() {
        //Categories
        for (Map.Entry<String, String> entry : mapLineCategories.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            listLineChartCategories.add(entry.getValue());
        }
    }

    private void generateLineChartDate() {
        //String dates
        for (Map.Entry<Date, String> entry : mapLineDate.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            listLineChartDate.add(entry.getValue());
        }
    }


    //Generate Line chart
    private void generateLineChart() {
        // Line chart
        int mapCount = 0;
        for (Map.Entry<String, ArrayList<Float>> entry : mapLineChart.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            ArrayList<Entry> lineValue = new ArrayList<>();
            int cntFloat = 0;
            for (float f : entry.getValue()) {
                lineValue.add(new Entry(cntFloat, (int) f));
                cntFloat = cntFloat + 1;
            }
            lineChartValue.add(lineValue);
        }

        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        int i = 0;
        for (ArrayList<Entry> entries : lineChartValue) {
            LineDataSet dataset = new LineDataSet(entries, "abv");
            int lineColor=activity.getResources().getColor(R.color.primaryRed);
            int circleColor=activity.getResources().getColor(R.color.color_badge);
            if(i==1){
                 lineColor=activity.getResources().getColor(R.color.color_item_selected);
                 circleColor=activity.getResources().getColor(R.color.purple_500);
            }
            dataset.setColors(lineColor);
            dataset.setCircleColor(circleColor);
            dataset.setMode(LineDataSet.Mode.LINEAR);

            dataset.setCircleRadius(5f);
            dataset.setAxisDependency(YAxis.AxisDependency.LEFT);
            dataset.setValueTextSize(10f);
            dataSets.add(dataset);
            i = i + 1;
        }
        lineData = new LineData(dataSets);
        Log.e("lineChartValue", String.valueOf(lineChartValue.size()));
    }

    //Generate Bar chart
    private void generateBarChart() {
        int mapCount = 0;
        for (Map.Entry<Date, ArrayList<Float>> entry : mapBarChart.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            float[] barData = new float[entry.getValue().size()];
            int cntFloat = 0;
            for (float f : entry.getValue()) {
                barData[cntFloat] = f;
                cntFloat = cntFloat + 1;
            }
            BarEntry barEntry = new BarEntry(mapCount, barData);
            entries.add(barEntry);
            mapCount = mapCount + 1;
        }

        BarDataSet barDataSet = new BarDataSet(entries, "");
        barDataSet.setValueTextSize(10f);
        barDataSet.setColors(getColors(listCategories.size()));
        barData = new BarData(barDataSet);
    }

    final Random mRandom = new Random(System.currentTimeMillis());

    public int generateRandomColor() {
        // This is the base color which will be mixed with the generated one
        final int baseColor = Color.WHITE;

        final int baseRed = Color.red(baseColor);
        final int baseGreen = Color.green(baseColor);
        final int baseBlue = Color.blue(baseColor);

        final int red = (baseRed + mRandom.nextInt(256)) / 2;
        final int green = (baseGreen + mRandom.nextInt(256)) / 2;
        final int blue = (baseBlue + mRandom.nextInt(256)) / 2;

        return Color.rgb(red, green, blue);
    }

    public int generateRandomDarkColor() {
        // This is the base color which will be mixed with the generated one
        final int baseColor = Color.WHITE;

        final int baseRed = Color.red(baseColor);
        final int baseGreen = Color.green(baseColor);
        final int baseBlue = Color.blue(baseColor);

        final int red = (baseRed + mRandom.nextInt(256)) * 2;
        final int green = (baseGreen + mRandom.nextInt(256)) * 2;
        final int blue = (baseBlue + mRandom.nextInt(256)) * 2;

        return Color.rgb(red, green, blue);
    }

    private int[] getColors(int size) {

//        // have as many colors as stack-values per entry
        int[] colors = new int[]{activity.getResources().getColor(R.color.color_item_selected)
        ,activity.getResources().getColor(R.color.color_Bar)};
//        int[] colors = new int[size];
//        for (int cnt = 0; cnt < colors.length; cnt++) {
//            colors[cnt] = generateRandomDarkColor();
//        }
        return colors;
    }

    public String getBarChartTitle() {
        return barChartTitle;
    }

    public String getLineChartTitle() {
        return lineChartTitle;
    }

    public String getProgChartTitle() {
        return progChartTitle;
    }
}
