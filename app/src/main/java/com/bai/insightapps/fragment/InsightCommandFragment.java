package com.bai.insightapps.fragment;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bai.insightapps.R;
import com.bai.insightapps.adapter.SectionsPagerAdapter;
import com.bai.insightapps.databinding.FragmentInsightCommandBinding;
import com.bai.insightapps.utilities.FileOperation;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InsightCommandFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InsightCommandFragment extends Fragment {

    FragmentInsightCommandBinding binding;
    SectionsPagerAdapter sectionsPagerAdapter;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public InsightCommandFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InsightCommandFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InsightCommandFragment newInstance(String param1, String param2) {
        InsightCommandFragment fragment = new InsightCommandFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_insight_command, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setFragmentAdapter();
    }

    private void setFragmentAdapter() {
        setTabLayout();
        FragmentManager fm = getChildFragmentManager();
        sectionsPagerAdapter = new SectionsPagerAdapter(fm);
        binding.pager.setAdapter(sectionsPagerAdapter);
        tabChangeTabSelectedListener();
    }

    private void setTabLayout() {
        binding.tabs.addTab(binding.tabs.newTab().setText(getResources().getString(R.string.tabMustKnow)));
        binding.tabs.addTab(binding.tabs.newTab().setText(getResources().getString(R.string.tabCommand)));
        binding.tabs.setTitleTypeFace(0, ResourcesCompat.getFont(getContext(), R.font.inter_semibold));
        binding.tabs.setTitleTypeFace(1, ResourcesCompat.getFont(getContext(), R.font.inter_regular));

        binding.tabs.setBadgeText(0, String.valueOf(FileOperation.getInsightReadCount()));
        binding.tabs.setBadgeText(1, String.valueOf(FileOperation.getCommandReadCount()));
        binding.tabs.setBadgeTextSize(getResources().getDimension(com.intuit.ssp.R.dimen._10ssp));

    }

    public Fragment getActiveFragment() {
        return sectionsPagerAdapter.getItem(binding.tabs.getSelectedTabPosition());
    }

    private void tabChangeTabSelectedListener() {

        binding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                assert view != null;
                TextView selectedText = view.findViewById(R.id.textview_tab_badge);
                selectedText.setTypeface(null, Typeface.BOLD);
                TextView selectedTitleText = view.findViewById(R.id.textview_tab_title);
                selectedTitleText.setTypeface(ResourcesCompat.getFont(getContext(), R.font.inter_semibold));
                binding.pager.setCurrentItem(tab.getPosition(), false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                assert view != null;
                TextView selectedText = view.findViewById(R.id.textview_tab_badge);
                selectedText.setTypeface(null, Typeface.NORMAL);
                TextView selectedTitleText = view.findViewById(R.id.textview_tab_title);
                selectedTitleText.setTypeface(ResourcesCompat.getFont(getContext(), R.font.inter_regular));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


}