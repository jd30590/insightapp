package com.bai.insightapps.fragment;

import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.bai.insightapps.R;
import com.bai.insightapps.adapter.DetailDataAdapter;
import com.bai.insightapps.databinding.FragmentDetailDialogBinding;
import com.bai.insightapps.model.Info;
import com.bai.insightapps.model.data.DataDetail;
import com.bai.insightapps.model.data.Detail;
import com.bai.insightapps.utilities.ConstantBundleKey;
import com.bai.insightapps.utilities.FileOperation;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DetailDialogFragment extends DialogFragment {
    FragmentDetailDialogBinding binding;
    DetailDataAdapter adapter;
    ArrayList<DataDetail> listData;
    String title = "";
    int sortCat = 0, sortName = 0, sortValue = 0;

    public static DetailDialogFragment getInstance(int messageId) {
        DetailDialogFragment fragment = new DetailDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ConstantBundleKey.MESSAGE_ID, messageId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int messageId = getArguments().getInt(ConstantBundleKey.MESSAGE_ID);
        Detail detail = FileOperation.getDataDetail();
//        if (detail.getMessageId() == messageId) {
//            listData = (ArrayList<DataDetail>) FileOperation.getDataDetail().getDataDetail();
//        } else {
//            listData = new ArrayList<>();
//        }
        listData = (ArrayList<DataDetail>) FileOperation.getDataDetail().getDataDetail();
        title = detail.getTitle();
        ExecutorService executors = Executors.newSingleThreadExecutor();
        Handler handler = new Handler();
        executors.execute(() -> {
            int cnt = 0;
            for (DataDetail detail1 : listData) {
                detail1.setValueTemp(Double.parseDouble(detail1.getValue()));
                listData.set(cnt, detail1);
                cnt = cnt + 1;
            }

            handler.post(() -> {
            });
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        dialogPercentage(90);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail_dialog
                , container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.tvDatailTitle.setText(title);
        setAdapter();
        setOnClickListener();
    }

    private void setOnClickListener() {
        binding.tvLblCat.setOnClickListener(clickListener);
        binding.tvLblName.setOnClickListener(clickListener);
        binding.tvLblValue.setOnClickListener(clickListener);
    }

    private void setAdapter() {
        if (listData.size() > 0) {
            adapter = new DetailDataAdapter(getContext(), listData);
            binding.rvDetailData.setAdapter(adapter);
            binding.rvDetailData.setVisibility(View.VISIBLE);
            binding.noDataView.setVisibility(View.GONE);
        } else {
            binding.rvDetailData.setVisibility(View.GONE);
            binding.noDataView.setVisibility(View.VISIBLE);
        }
    }

    void dialogPercentage(int percentage) {
        float percent = (float) percentage / 100;
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        Rect rect = new Rect(0, 0, metrics.widthPixels, metrics.heightPixels);
        float percentWidth = rect.width() * percent;
        getDialog().getWindow().setLayout((int) percentWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tvLblCat:

                    sortByCategory();

                    break;
                case R.id.tvLblName:

                    sortByName();

                    break;
                case R.id.tvLblValue:

                    sortByValue();

                    break;
            }
        }
    };

    private void setDrableTint(TextView selectTextview, TextView unSelectedTextview1, TextView unselectedTextview2) {
        for (Drawable drawable : selectTextview.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(selectTextview.getContext(), R.color.primaryRed), PorterDuff.Mode.SRC_IN));
            }
        }

        for (Drawable drawable : unSelectedTextview1.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(unSelectedTextview1.getContext(), R.color.black), PorterDuff.Mode.SRC_IN));
            }
        }

        for (Drawable drawable : unselectedTextview2.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(unselectedTextview2.getContext(), R.color.black), PorterDuff.Mode.SRC_IN));
            }
        }
    }

    private void setTextviewDrawable(TextView selectTextview, TextView unSelectedTextview1, TextView unselectedTextview2, int sort) {
        if (sort == 0) {
            selectTextview.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_table_down, 0);
        } else {
            selectTextview.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_table_up, 0);
        }
        unSelectedTextview1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_table_down, 0);
        unselectedTextview2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_table_down, 0);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void sortByCategory() {
        sortValue = 0;
        sortName = 0;
        if (sortCat == 0) {
            Collections.sort(listData, compareByCategory);
            sortCat = 1;
        } else {
            Collections.sort(listData,
                    Collections.reverseOrder(compareByCategory));
            sortCat = 0;
        }
        adapter.notifyDataSetChanged();
        setTextviewDrawable(binding.tvLblCat, binding.tvLblName, binding.tvLblValue, sortCat);
        setDrableTint(binding.tvLblCat, binding.tvLblName, binding.tvLblValue);
    }

    Comparator<DataDetail> compareByCategory = new Comparator<DataDetail>() {
        @Override
        public int compare(DataDetail o1, DataDetail o2) {
            return o1.getCategori().compareTo(o2.getCategori());
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void sortByName() {
        sortCat = 0;
        sortValue = 0;
        if (sortName == 0) {
            Collections.sort(listData, compareByName);
            sortName = 1;
        } else {
            listData.sort(Collections.reverseOrder(compareByName));
            sortName = 0;
        }
        adapter.notifyDataSetChanged();
        setTextviewDrawable(binding.tvLblName, binding.tvLblCat, binding.tvLblValue, sortName);
        setDrableTint(binding.tvLblName, binding.tvLblCat, binding.tvLblValue);
    }

    Comparator<DataDetail> compareByName = new Comparator<DataDetail>() {
        @Override
        public int compare(DataDetail o1, DataDetail o2) {
            return o1.getName().compareTo(o2.getName());
        }
    };


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void sortByValue() {
        sortCat = 0;
        sortName = 0;
        if (sortValue == 0) {
            Collections.sort(listData, compareByValue);
            sortValue = 1;
        } else {
            listData.sort(Collections.reverseOrder(compareByValue));
            sortValue = 0;
        }
        adapter.notifyDataSetChanged();
        setTextviewDrawable(binding.tvLblValue, binding.tvLblCat, binding.tvLblName, sortValue);
        setDrableTint(binding.tvLblValue, binding.tvLblCat, binding.tvLblName);
    }

    Comparator<DataDetail> compareByValue = new Comparator<DataDetail>() {
        @Override
        public int compare(DataDetail o1, DataDetail o2) {
            return Double.compare(o1.getValueTemp(), o2.getValueTemp());
        }
    };
}
