package com.bai.insightapps.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bai.insightapps.R;
import com.bai.insightapps.activities.CommandActivity;
import com.bai.insightapps.adapter.CommandAdapter;
import com.bai.insightapps.adapter.InsightAdapter;
import com.bai.insightapps.databinding.FragmentCommandBinding;
import com.bai.insightapps.interfaces.SetOnItemClick;
import com.bai.insightapps.model.Info;
import com.bai.insightapps.utilities.ActivityUtils;
import com.bai.insightapps.utilities.ConstantBundleKey;
import com.bai.insightapps.utilities.FileOperation;

import java.util.ArrayList;

public class CommandFragment extends Fragment {
    FragmentCommandBinding binding;
    ArrayList<Info> listCommand;
    CommandAdapter commandAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listCommand = FileOperation.getListCommand();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater
                , R.layout.fragment_command
                , container
                , false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAdapter(listCommand);
    }

    private void setAdapter(ArrayList<Info> listInfoCommand) {
        commandAdapter = new CommandAdapter(listInfoCommand, infoSetOnItemClick);
        binding.rvCommand.setAdapter(commandAdapter);
    }

    SetOnItemClick<Info> infoSetOnItemClick = (position, Item) -> {
        //  Toast.makeText(getContext(), "Item " + Item.getMsgDate(), Toast.LENGTH_SHORT).show();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ConstantBundleKey.CONSTANT_DATA, Item);
        bundle.putInt(ConstantBundleKey.MESSAGE_ID, Item.getMessageId());
        ActivityUtils.launchActivity(getActivity()
                , CommandActivity.class
                , false
                , bundle);
    };

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
                new IntentFilter(ConstantBundleKey.ACTION_SORT_COMMAND));
    }

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
        super.onPause();
    }

    // Our handler for received Intents. This will be called whenever an Intent
// with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            ArrayList<Info> listData = (ArrayList<Info>) intent.getSerializableExtra(ConstantBundleKey.COMMAND_DATA);

            FileOperation.setListCommandFilter(listData);

            listCommand = listData;
            setAdapter(listCommand);
        }
    };
}