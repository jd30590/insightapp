package com.bai.insightapps.interfaces;

public interface SetOnItemClick<T> {
    void onItemClick(int position, T Item);
}
