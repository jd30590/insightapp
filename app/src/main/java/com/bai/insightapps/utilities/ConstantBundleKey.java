package com.bai.insightapps.utilities;

import android.content.IntentFilter;

public class ConstantBundleKey {
    public static final String CONSTANT_DATA = "DATA";
    public static final String MESSAGE_ID = "MESSAGE_ID";
    public static final String CATEGORY1 = "CATEGORY1";
    public static final String CATEGORY2 = "CATEGORY2";
    public static final String DAILY = "Daily";
    public static final String WEEKLY = "Weekly";
    public static final String MONTHLY = "Monthly";
    public static final String TITLE = "TITLE";
    public static final String COMMAND_DATA = "commandData";
    public static final String ACTION_SORT_COMMAND = "SORT_COMMAND";
    public static final String DAILY_DATA = "DAILY_DATA";
    public static final String WEEKLY_DATA = "WEEKLY_DATA";
    public static final String MONTHLY_DATA = "MONTHLY_DATA";
}
