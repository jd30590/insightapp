package com.bai.insightapps.utilities;

import android.content.Context;
import com.bai.insightapps.R;
import com.bai.insightapps.model.SliderItem;
import java.util.ArrayList;

public class SliderAdapterItem {

    public static ArrayList<SliderItem> getSliderItem(Context context) {
        ArrayList<SliderItem> listSliderItem = new ArrayList<>();
        SliderItem item = new SliderItem();
        item.setId(Constants.SLIDER_HOME);
        item.setSelected(1);
        item.setTitle(context.getResources().getString(R.string.lbl_slider_home));
        item.setImageRes(R.drawable.ic_slide_home);
        listSliderItem.add(item);

        item = new SliderItem();
        item.setId(Constants.SLIDER_PLAN);
        item.setSelected(0);
        item.setTitle(context.getResources().getString(R.string.lbl_slider_plan));
        item.setImageRes(R.drawable.ic_slide_bar);
        listSliderItem.add(item);

        item = new SliderItem();
        item.setId(Constants.SLIDER_PROGRESS);
        item.setSelected(0);
        item.setTitle(context.getResources().getString(R.string.lbl_slider_progress));
        item.setImageRes(R.drawable.ic_slide_progress);
        listSliderItem.add(item);

        item = new SliderItem();
        item.setId(Constants.SLIDER_SYNC);
        item.setSelected(0);
        item.setTitle(context.getResources().getString(R.string.lbl_slider_sync));
        item.setImageRes(R.drawable.ic_slider_sync);
        listSliderItem.add(item);

        item = new SliderItem();
        item.setId(Constants.SLIDER_SETTING);
        item.setSelected(0);
        item.setTitle(context.getResources().getString(R.string.lbl_slider_setting));
        item.setImageRes(R.drawable.ic_slider_settings);
        listSliderItem.add(item);

        item = new SliderItem();
        item.setId(Constants.SLIDER_LOGOUT);
        item.setSelected(0);
        item.setTitle(context.getResources().getString(R.string.lbl_slider_logout));
        item.setImageRes(R.drawable.ic_slide_logout);
        listSliderItem.add(item);

        return listSliderItem;
    }

}
