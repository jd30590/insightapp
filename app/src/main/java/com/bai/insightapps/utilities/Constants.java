package com.bai.insightapps.utilities;

public class Constants {
    public static final String PREFNAME = "InsightApp";
    public static final String PREF_IS_LOGIN = "isLogin";
    public static final int SLIDER_HOME = 1;
    public static final int SLIDER_PLAN = 2;
    public static final int SLIDER_PROGRESS = 3;
    public static final int SLIDER_SYNC = 4;
    public static final int SLIDER_SETTING = 5;
    public static final int SLIDER_LOGOUT = 6;
    public static final String DATE_FORMATTER = "dd MMMM yyyy";
    public static final String MONTH_YEAR_FORMATTER = "MMMM yyyy";
    public static final String CHART_DATE_FORMAT = "yyyy-MM-dd";
    public static final String INSIGHT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SS";
}
