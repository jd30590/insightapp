package com.bai.insightapps.utilities;

import android.app.Application;
import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

public class InsightsApp extends Application {

    public static FileOperation operation;
    private static InsightsApp myApplication;
    private static Context context;


    @RequiresApi(api = Build.VERSION_CODES.N)
    public static FileOperation getOperation() {
        operation = FileOperation.getInstance();
        return operation;
    }

    public static InsightsApp getInstance() {
        if (myApplication == null) {
            myApplication = new InsightsApp();
        }
        return myApplication;
    }

    public static Context getAppContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (context == null) {
            context = getApplicationContext();
        }
    }
}
