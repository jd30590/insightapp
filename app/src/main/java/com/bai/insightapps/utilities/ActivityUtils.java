package com.bai.insightapps.utilities;

import android.app.Activity;

import android.content.Context;

import android.content.Intent;

import android.os.Bundle;


import androidx.core.app.ActivityCompat;

import androidx.fragment.app.Fragment;

public class ActivityUtils {
    public static void launchActivity(Activity context, Class<? extends Activity> activity,
                                      boolean closeCurrentActivity, Bundle bundle) {
        Intent intent = new Intent(context, activity);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivity(intent);
        if (closeCurrentActivity) {
            context.finish();
        }
    }


    public static void launchActivity(Activity context, Class<? extends Activity> activity,
                                      boolean closeCurrentActivity, Bundle bundle, boolean isResultForward) {

        Intent intent = new Intent(context, activity);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        if (isResultForward) {
            intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        }
        context.startActivity(intent);
        if (closeCurrentActivity) {
            context.finish();
        }
    }

    public static void launchActivity(Context context, Class<? extends Activity> activity,
                                      Bundle bundle) {
        Intent intent = new Intent(context, activity);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivity(intent);
    }

    public static void launchActivity(Activity context, Class<? extends Activity> activity) {
        Intent intent = new Intent(context, activity);
        context.startActivity(intent);
        ActivityCompat.finishAffinity(context);
    }

    public static void launchActivity(Activity context, Class<? extends Activity> activity,
                                      boolean closeCurrentActivity) {
        ActivityUtils.launchActivity(context, activity, closeCurrentActivity, null);
    }

    public static void launchStartActivityForResult(Activity context
            , Class<? extends Activity> activity
            , boolean closeCurrentActivity
            , Bundle bundle
            , int requestCode) {
        Intent intent = new Intent(context, activity);
        if (bundle != null) {
            intent.putExtras(bundle);
        }

        context.startActivityForResult(intent, requestCode);
        if (closeCurrentActivity) {
            context.finish();
        }
    }


    public static void launchStartActivityForResult(Activity context, Class<? extends Activity> activity, Bundle bundle, int requestCode) {
        Intent intent = new Intent(context, activity);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivityForResult(intent, requestCode);
    }


    public static void launchStartActivityForResult(Fragment context, Class<? extends Activity> activity, Bundle bundle, int requestCode) {
        Intent intent = new Intent(context.getContext(), activity);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        context.startActivityForResult(intent, requestCode);
    }

    public static void launchStartActivityForResult(
            Fragment fragment, Class<? extends Activity> activity
            , int requestCode, Bundle bdl) {
        Intent intent = new Intent(fragment.getContext(), activity);
        if (bdl != null)
            intent.putExtras(bdl);
        fragment.startActivityForResult(intent, requestCode);
    }

    public static void launchActivityWithClearBackStack(Context context,
                                                        Class<? extends Activity> activity) {
        Intent intent = new Intent(context, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }

    public static void launchActivityWithClearBackStack(Context context,
                                                        Class<? extends Activity> activity,
                                                        Bundle bundle) {

        Intent intent = new Intent(context, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                Intent.FLAG_ACTIVITY_CLEAR_TASK);
        if (bundle != null)
            intent.putExtras(bundle);
        context.startActivity(intent);
    }
}
