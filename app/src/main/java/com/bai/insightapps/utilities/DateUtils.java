package com.bai.insightapps.utilities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.text.format.DateFormat;
import android.widget.TimePicker;


import com.bai.insightapps.R;
import com.bai.insightapps.interfaces.OnDateChange;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;


public class DateUtils {

    public static final String DD_MM_YYYY = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String YYYY_DD_MM = "yyyy-MM-dd";
    public static final String DDMMYYYY = "dd-MM-yyyy";
    public static final String DD_MMM_YYYY = "dd MMM yyyy";
    public static final String DATENOTIFICATION = "dd MMM, HH:mm";

    public static DatePickerDialog openDatePickerDialog(Context context, Calendar calendar,
                                                        final OnDateChange onDateChange) {
        return new DatePickerDialog(context, R.style.DatePickerDialogTheme
                , (view, year, monthOfYear, dayOfMonth) -> onDateChange.onDateSet(view, year, monthOfYear, dayOfMonth)
                , calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static TimePickerDialog openTimePickerDialog(Context context, Calendar calendar,
                                                        final OnDateChange onDateChange) {
        return new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                onDateChange.onTimeSet(timePicker, selectedHour, selectedMinute);
            }
        }, calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), true);
    }


    public static int getDaysBetweenTwoDates(Timestamp start, Timestamp end) {
        boolean negative = false;
        if (end.before(start)) {
            negative = true;
            Timestamp temp = start;
            start = end;
            end = temp;
        }

        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(start);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        GregorianCalendar calEnd = new GregorianCalendar();
        calEnd.setTime(end);
        calEnd.set(Calendar.HOUR_OF_DAY, 0);
        calEnd.set(Calendar.MINUTE, 0);
        calEnd.set(Calendar.SECOND, 0);
        calEnd.set(Calendar.MILLISECOND, 0);

        if (cal.get(Calendar.YEAR) == calEnd.get(Calendar.YEAR)) {
            if (negative) {
                return (calEnd.get(Calendar.DAY_OF_YEAR) - cal.get(Calendar.DAY_OF_YEAR)) * -1;
            }
            return calEnd.get(Calendar.DAY_OF_YEAR) - cal.get(Calendar.DAY_OF_YEAR);
        }

        int days = 0;
        while (calEnd.after(cal)) {
            cal.add(Calendar.DAY_OF_YEAR, 1);
            days++;
        }
        if (negative) {
            return days * -1;
        }
        return days;
    }

    public static int getDaysBetween(Timestamp end) {
        return getDaysBetweenTwoDates(new Timestamp(System.currentTimeMillis()), end);
    }


    public static boolean validTime(String dateTime) {
        Date reminderDate = DateUtils.getDateHour(dateTime);
        Date currentDate = Calendar.getInstance().getTime();
        return reminderDate.after(currentDate);
    }

    public static String get12HourTime(String time) {
        SimpleDateFormat displayFormat = new SimpleDateFormat("h:mm aa", Locale.getDefault());
        displayFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat parseFormat = new SimpleDateFormat("H:mm", Locale.getDefault());
        parseFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = parseFormat.parse(time);
        } catch (ParseException e) {
            date = new Date();
        }
        return displayFormat.format(date);
    }


    public static String get24HourTime(String time) {

        SimpleDateFormat displayFormat = new SimpleDateFormat("H:mm", Locale.getDefault());
        displayFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat parseFormat = new SimpleDateFormat("h:mm aa", Locale.getDefault());
        parseFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = parseFormat.parse(time);
        } catch (ParseException e) {
            date = new Date();
        }
        return displayFormat.format(date);
    }


    public static Date getDate(String d) {
        SimpleDateFormat format = new SimpleDateFormat(DD_MM_YYYY, Locale.getDefault());
        Date date = null;
        try {
            date = format.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static long getDateLong(String d) {
        SimpleDateFormat format = new SimpleDateFormat(DD_MM_YYYY, Locale.getDefault());
        Date date = null;
        try {
            date = format.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public static String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        return DateFormat.format(DD_MM_YYYY, cal).toString();
    }

    public static String getDateBasedOnFormat(long time, String format) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        return DateFormat.format(format, cal).toString();
    }


    public static String getDateDDMMYYYY(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        return DateFormat.format(DDMMYYYY, cal).toString();
    }

    public static String getDateDDMMYYYY(long time, String format) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        return DateFormat.format(format, cal).toString();
    }

    private static Date getDateHour(String d) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
        Date date = null;
        try {
            date = format.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static String parseDate(String pFormat, String dFormat, String date) {
        SimpleDateFormat displayFormat = new SimpleDateFormat(dFormat, Locale.getDefault());
        displayFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat parseFormat = new SimpleDateFormat(pFormat, Locale.getDefault());
        parseFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date d = null;
        try {
            d = parseFormat.parse(date);
        } catch (ParseException e) {
            d = new Date();
        }
        return displayFormat.format(d);
    }

    public static String getCalculatedDate(String startDate, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(DD_MM_YYYY, Locale.getDefault());
        Date date = null;
        try {
            date = s.parse(startDate);
        } catch (ParseException e) {
            date = new Date();
        }
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, days);
        return s.format(new Date(cal.getTimeInMillis()));
    }

    public static ArrayList<String> getPreviousDays(int previousDays, int nextDays) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM", Locale.getDefault());
        ArrayList<String> daysList = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        for (int i = 0; i < previousDays; i++) {
            cal.add(Calendar.DAY_OF_MONTH, -1);

            String date = sdf.format(cal.getTime());
            daysList.add(date);
        }
        Collections.reverse(daysList);
        cal = Calendar.getInstance();
        String d = sdf.format(cal.getTime());
        daysList.add(d);

        for (int i = 0; i < nextDays; i++) {
            cal.add(Calendar.DAY_OF_MONTH, 1);
            String date = sdf.format(cal.getTime());
            daysList.add(date);
        }
        //  Collections.reverse(daysList);
        return daysList;
    }


    public static boolean compareTwoMiliSec(long curLong, long authLong) {
        boolean isGreter = false;
        Date curdate = new Date(curLong);
        Date authDate = new Date(authLong);


        // Date class also has their own before() and after() method
        if (authDate.after(curdate)) {
            isGreter = true;
        } else if (authDate.equals(curdate)) {
            isGreter = true;
        } else {
            isGreter = false;
        }
        return isGreter;
    }

    public static String getTime(long milliseconds) {
        try {

            int offset = TimeZone.getDefault().getRawOffset() + TimeZone.getDefault().getDSTSavings();

            Calendar smsTime = Calendar.getInstance();

            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            inputFormat.setTimeZone(smsTime.getTimeZone());

            smsTime.setTimeInMillis((milliseconds * 1000));// + offset);

            Calendar now = Calendar.getInstance();

            final String timeFormatString = "h:mm aa";
            final String dateTimeFormatString = "MMM d, h:mm aa";

            if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
                return DateFormat.format(timeFormatString, smsTime).toString();

            } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
                return DateFormat.format(timeFormatString, smsTime).toString();

            } else if (now.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)) {
                return DateFormat.format(timeFormatString, smsTime).toString();

            } else {
                return DateFormat.format(timeFormatString, smsTime).toString();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getDatemmyy(long millis) {
        //creating Date from millisecond
        Date currentDate = new Date(millis);
        String convertedDate = "";
        try {
            SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
            //formatted value of current Date
            convertedDate = df.format(currentDate);
            convertedDate = convertedDate.replaceAll("-", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public static String getDateyyyymmdd(long millis) {
        //creating Date from millisecond
        Date currentDate = new Date(millis);
        String convertedDate = "";
        try {
            SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
            //formatted value of current Date
            convertedDate = df.format(currentDate);
            convertedDate = convertedDate.replaceAll("-", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertedDate;
    }

    public static String getConvertedDate(Context context, long smsTimeInMilis) {

        Calendar cal = Calendar.getInstance();
        Calendar oldCal = Calendar.getInstance();
        oldCal.setTimeInMillis(smsTimeInMilis);

        int oldYear = oldCal.get(Calendar.YEAR);
        int year = cal.get(Calendar.YEAR);
        int oldDay = oldCal.get(Calendar.DAY_OF_YEAR);
        int day = cal.get(Calendar.DAY_OF_YEAR);

        Date oldDate = new Date(oldCal.getTimeInMillis());
        Date result = null;
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MONTH, -1);
            calendar.add(Calendar.DAY_OF_MONTH, -7);
            result = calendar.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (oldYear == year) {
            int previousDaysValue = day - oldDay;
            if (previousDaysValue == 1) {
                return context.getResources().getString(R.string.strYesterday);
            } else if (previousDaysValue == 0) {
                return context.getResources().getString(R.string.strToday);
            } else if (previousDaysValue < 7) {
                return DateFormat.format("EEEE", oldCal).toString();
            } else if (result != null && result.before(oldDate)) {
                return DateFormat.format("EEE, dd MMM", oldCal).toString();
            } else {
                return DateFormat.format("dd/MM/yyyy", oldCal).toString();
            }
        } else {
            return DateFormat.format("dd/MM/yyyy", oldCal).toString();
        }
    }

    private static SimpleDateFormat yyyy_MM_dd_HH_mm = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm", Locale.getDefault());
    private static SimpleDateFormat HHmm = new SimpleDateFormat("HH:mm",
            Locale.getDefault());
    private static SimpleDateFormat MM_dd_HHmm = new SimpleDateFormat(
            "MM-dd HH:mm", Locale.getDefault());

    public String dateConversation(String createAt, String simpleDateFormate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(simpleDateFormate);
        Date date = null;//You will get date object relative to server/client timezone wherever it is parsed
        try {
            date = dateFormat.parse(createAt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm aa"); //If you need time just put specific format for time like 'HH:mm:ss'
        return formatter.format(date);
    }

    public static String dateConversationChannel(String createAt, String simpleDateFormate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(simpleDateFormate);
        Date date = null;//You will get date object relative to server/client timezone wherever it is parsed
        try {
            date = dateFormat.parse(createAt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yy"); //If you need time just put specific format for time like 'HH:mm:ss'
        Calendar calendarChannel = Calendar.getInstance();
        calendarChannel.setTime(date);

        if (android.text.format.DateUtils.isToday(calendarChannel.getTimeInMillis())) {
            return "Today";
        } else if (android.text.format.DateUtils.isToday(calendarChannel.getTimeInMillis()
                + android.text.format.DateUtils.DAY_IN_MILLIS)) {
            return "Yesterday";
        } else {
            return formatter.format(date);
        }


    }
}