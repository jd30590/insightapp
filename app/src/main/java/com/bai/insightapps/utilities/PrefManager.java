package com.bai.insightapps.utilities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import java.util.Set;

/**
 * Local preference manager, to store all user's information and one time load information
 * into local preference of Balador
 */
public class PrefManager {

    public static final String CURRENT_SYSTEM_TIME = "CURRENT_SYSTEM_TIME";
    public static final String ANNOUNCEMENT = "Announcement";
    public static final String IS_ANNOUNCEMENT = "isAnnouncement";
    public static final String IS_ADMIN = "isAdmin";
    public static String UserPostId = "USER_POST_ID";
    private SharedPreferences preferences;
    private static PrefManager singleton = null;
    private SharedPreferences.Editor editor;

    PrefManager(Context context) {
        preferences = context.getSharedPreferences(
                Constants.PREFNAME, Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
    }

    public static PrefManager getInstance() {
        if (singleton == null) {
            singleton = new Builder(InsightsApp.getAppContext()).build();
        }
        return singleton;
    }


    private static class Builder {

        private final Context context;

        Builder(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.context = context.getApplicationContext();
        }

        PrefManager build() {
            return new PrefManager(context);
        }
    }

    public void save(String key, boolean value) {
        editor.putBoolean(key, value).apply();
    }

    public void remove(String key) {
        editor.remove(key).apply();
    }


    public void save(String key, String value) {
        editor.putString(key, value).apply();
    }

    public void save(String key, int value) {
        editor.putInt(key, value).apply();
    }

    public void save(String key, float value) {
        editor.putFloat(key, value).apply();
    }

    public void save(String key, long value) {
        editor.putLong(key, value).apply();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void save(String key, Set<String> value) {
        editor.putStringSet(key, value).apply();
    }

    public boolean getBoolean(String key, boolean defValue) {
        return preferences.getBoolean(key, defValue);
    }

    public String getString(String key, String defValue) {
        return preferences.getString(key, defValue);
    }

    public int getInt(String key, int defValue) {
        return preferences.getInt(key, defValue);
    }

    public float getFloat(String key, float defValue) {
        return preferences.getFloat(key, defValue);
    }

    public long getLong(String key, long defValue) {
        return preferences.getLong(key, defValue);
    }
}
