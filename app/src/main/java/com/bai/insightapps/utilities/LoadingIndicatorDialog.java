package com.bai.insightapps.utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;

import com.bai.insightapps.R;

public class LoadingIndicatorDialog {

    ProgressDialog progressDialog;
    Activity activity;

    public LoadingIndicatorDialog(Activity activity) {
        this.activity = activity;
        showProgressDialog();
    }

    public void showProgressDialog() {
        progressDialog = new ProgressDialog(activity,R.style.Theme_InsightApps);
        progressDialog.setContentView(R.layout.layout_progress_dialog);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    public void show() {
        progressDialog.show();
    }

    public void dismiss() {
        progressDialog.dismiss();
    }
}
