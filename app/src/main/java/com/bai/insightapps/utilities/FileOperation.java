package com.bai.insightapps.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.bai.insightapps.R;
import com.bai.insightapps.model.Info;
import com.bai.insightapps.model.ListInfoModel;
import com.bai.insightapps.model.data.Detail;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public class FileOperation {
    private static FileOperation singleton = null;

    public static ListInfoModel infoModel = new ListInfoModel();
    public static ListInfoModel infoModelCommand = new ListInfoModel();
    public static ArrayList<Info> listCommand = new ArrayList<>();
    public static ArrayList<Info> listInsight = new ArrayList<>();
    public static ArrayList<Info> listDailyInsight = new ArrayList<>();
    public static ArrayList<Info> listWeeklyInsight = new ArrayList<>();
    public static ArrayList<Info> listMonthlyInsight = new ArrayList<>();
    public static ArrayList<Info> listCommandFilter = new ArrayList<>();
    public static ArrayList<Info> listDailyInsightFilter = new ArrayList<>();
    public static ArrayList<Info> listWeeklyInsightFilter = new ArrayList<>();
    public static ArrayList<Info> listMonthlyInsightFilter = new ArrayList<>();
    public static LinkedHashMap<String, String> linkedHashMapTopic = new LinkedHashMap<>();
    public static int insightReadCount = 0;
    public static int commandReadCount = 0;
    public static Detail dataDetail = new Detail();
    public static com.bai.insightapps.model.chart.Detail chartDetail = new com.bai.insightapps.model.chart.Detail();
    private static Context context;

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static FileOperation getInstance() {
        if (singleton == null) {
            singleton = new FileOperation(InsightsApp.getAppContext());
        }
        return singleton;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    FileOperation(Context context) {
        this.context = context;
        readInsightFile(context);
        readCommandFile(context);
        readDataDetail(context);
        readChartDetail(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void readInsightFile(Context context) {
        Gson gson = new Gson();
        String listInfoString = Utils.getJsonFromAssets(context, "1_listinfoInsight_json3.json");
        infoModel = gson.fromJson(listInfoString, ListInfoModel.class);
        listInsight.clear();
        listInsight.addAll(infoModel.getListInfo());
        manipulateInsightList();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void readCommandFile(Context context) {
        Gson gson = new Gson();
        String listInfoCommand = Utils.getJsonFromAssets(context, "1_listinfoCommand_json2.json");
        infoModelCommand = gson.fromJson(listInfoCommand, ListInfoModel.class);
        listCommand.clear();
        listCommand.addAll(infoModelCommand.getListInfo());
        manipulateCommandList();
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public void readDataDetail(Context context) {
        Gson gson = new Gson();
        String dataDetailStr = Utils.getJsonFromAssets(context, "2_datadetail_json2.json");
        dataDetail = gson.fromJson(dataDetailStr, Detail.class);
        Log.e("DataDetail", dataDetail.getTitle());
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void readChartDetail(Context context) {
        Gson gson = new Gson();
        String chartDetailStr = Utils.getJsonFromAssets(context, "3_chartdetail_json2.json");
        chartDetail = gson.fromJson(chartDetailStr, com.bai.insightapps.model.chart.Detail.class);
        Log.e("ChartDetail", "" + chartDetail.getChartDetail().size());
    }

    public static Detail getDataDetail() {
        return dataDetail;
    }

    public static com.bai.insightapps.model.chart.Detail getChartDetail() {
        return chartDetail;
    }

    public ListInfoModel getInfoModel() {
        return infoModel;
    }

    public static ListInfoModel getInfoModelCommand() {
        return infoModelCommand;
    }

    public static ArrayList<Info> getListCommand() {
        return listCommand;
    }

    public ArrayList<Info> getListInsight() {
        return listInsight;
    }

    public static ArrayList<Info> getListDailyInsight() {
        return listDailyInsight;
    }

    public static ArrayList<Info> getListWeeklyInsight() {
        return listWeeklyInsight;
    }

    public static ArrayList<Info> getListMonthlyInsight() {
        return listMonthlyInsight;
    }

    public static int getInsightReadCount() {
        return insightReadCount;
    }

    public static int getCommandReadCount() {
        return commandReadCount;
    }

    public static ArrayList<Info> getListCommandFilter() {
        return listCommandFilter;
    }

    public static ArrayList<Info> getListDailyInsightFilter() {
        return listDailyInsightFilter;
    }

    public static ArrayList<Info> getListWeeklyInsightFilter() {
        return listWeeklyInsightFilter;
    }

    public static ArrayList<Info> getListMonthlyInsightFilter() {
        return listMonthlyInsightFilter;
    }

    public static void setListCommandFilter(ArrayList<Info> listCommandFilter) {
        FileOperation.listCommandFilter = listCommandFilter;
    }

    public static void setListDailyInsightFilter(ArrayList<Info> listDailyInsightFilter) {
        FileOperation.listDailyInsightFilter = listDailyInsightFilter;
    }

    public static void setListWeeklyInsightFilter(ArrayList<Info> listWeeklyInsightFilter) {
        FileOperation.listWeeklyInsightFilter = listWeeklyInsightFilter;
    }

    public static void setListMonthlyInsightFilter(ArrayList<Info> listMonthlyInsightFilter) {
        FileOperation.listMonthlyInsightFilter = listMonthlyInsightFilter;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void manipulateInsightList() {
        int i = 0;
        for (Info info : listInsight) {
            if (!info.getIsRead())
                insightReadCount = insightReadCount + 1;
            Date msgDate = null;
            try {
                msgDate = stringToDate(info.getMsgTime(), Constants.INSIGHT_DATE_FORMAT);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            info.setMsgDate(msgDate);
            linkedHashMapTopic.put(info.getTopic(), info.getTopic());
            listInsight.set(i, info);
            i++;
        }

       /* Collections.sort(listInsight, Comparator.comparing(Info::getIsRead)
                .thenComparing(Info::getPeriod));*/

        for (Info info : listInsight) {
            Log.e("getIsRead", "msg ID " + info.getMessageId()
                    + " Topic " + info.getTopic() + " isRead " + info.getIsRead());
            if (info.getPeriod().equalsIgnoreCase("Daily")) {
                listDailyInsight.add(info);
                listDailyInsightFilter.add(info);

            } else if (info.getPeriod().equalsIgnoreCase("Weekly")) {
                listWeeklyInsight.add(info);
                listWeeklyInsightFilter.add(info);
            } else if (info.getPeriod().equalsIgnoreCase("Monthly")) {
                listMonthlyInsight.add(info);
                listMonthlyInsightFilter.add(info);
            }
        }

        //Sort Daily Insight
        Collections.sort(listDailyInsight, dateCmdSortAsc);
        Collections.sort(listDailyInsight, topicCmdSortAsc);
        Collections.sort(listDailyInsight, readStatusCmdAsc);

        //Sort Weekly insight
        Collections.sort(listWeeklyInsight, dateCmdSortAsc);
        Collections.sort(listWeeklyInsight, topicCmdSortAsc);
        Collections.sort(listWeeklyInsight, readStatusCmdAsc);

        //Sort monthly insight
        Collections.sort(listMonthlyInsight, dateCmdSortAsc);
        Collections.sort(listMonthlyInsight, topicCmdSortAsc);
        Collections.sort(listMonthlyInsight, readStatusCmdAsc);


     /*   Collections.sort(listDailyInsight, Comparator.comparing(Info::getMsgDate)
                .thenComparing(Info::getTopic).thenComparing(Info::getIsRead));
        Collections.sort(listWeeklyInsight, Comparator.comparing(Info::getMsgDate)
                .thenComparing(Info::getTopic).thenComparing(Info::getIsRead));
        Collections.sort(listMonthlyInsight, Comparator.comparing(Info::getMsgDate)
                .thenComparing(Info::getTopic).thenComparing(Info::getIsRead));*/
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void manipulateCommandList() {
        int i = 0;
        for (Info info : listCommand) {
            if (!info.getIsRead())
                commandReadCount = commandReadCount + 1;
            Date msgDate = null;
            try {
                msgDate = stringToDate(info.getMsgTime(), Constants.INSIGHT_DATE_FORMAT);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            info.setMsgDate(msgDate);
            linkedHashMapTopic.put(info.getTopic(), info.getTopic());
            listCommand.set(i, info);
            listCommandFilter.add(info);
            i++;
        }
        Collections.sort(listCommand, dateCmdSortAsc);
        Collections.sort(listCommand, topicCmdSortAsc);
        Collections.sort(listCommand, readStatusCmdAsc);

//        Collections.sort(listCommand, Comparator.comparing(Info::getMsgDate)
//                .thenComparing(Info::getTopic).thenComparing(Info::getIsRead));
//        Collections.sort(listCommand);
        for (Info info : listCommand) {
            Log.e("getIsReadCMD", "msg ID " + info.getMessageId()
                    + " Topic " + info.getTopic() + " isRead " + info.getIsRead());
        }
    }

    Comparator<Info> dateCmdSortAsc = new Comparator<Info>() {
        @Override
        public int compare(Info info1, Info info2) {
            return Long.compare(info1.getMsgDate().getTime(), info2.getMsgDate().getTime());
        }
    };
    Comparator<Info> topicCmdSortAsc = new Comparator<Info>() {
        @Override
        public int compare(Info info1, Info info2) {
            return info1.getTopic().compareTo(info2.getTopic());
        }
    };

    Comparator<Info> readStatusCmdAsc = new Comparator<Info>() {
        @Override
        public int compare(Info info1, Info info2) {
            return info1.getIsRead().compareTo(info1.getIsRead());
        }
    };

    public static Date stringToDate(String aDate, String aFormat) throws ParseException {
        SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
        Date stringDate = simpledateformat.parse(aDate);
        return stringDate;
    }

    public static ArrayList<String> getListTopic() {
        ArrayList<String> listTopic = new ArrayList<>();
        listTopic.clear();
        listTopic.add(0, context.getResources().getString(R.string.lblSelTopic));
        for (Map.Entry<String, String> entry : linkedHashMapTopic.entrySet()) {
            System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            listTopic.add(entry.getValue());
        }
        return listTopic;
    }
}

