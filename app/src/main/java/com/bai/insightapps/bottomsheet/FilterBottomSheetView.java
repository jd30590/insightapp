package com.bai.insightapps.bottomsheet;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.TimePicker;

import androidx.annotation.RequiresApi;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bai.insightapps.R;
import com.bai.insightapps.activities.MainActivity;
import com.bai.insightapps.databinding.LayoutFilterBottomSheetBinding;
import com.bai.insightapps.databinding.LoyoutBottomSortBinding;
import com.bai.insightapps.interfaces.OnDateChange;
import com.bai.insightapps.model.Info;
import com.bai.insightapps.utilities.ConstantBundleKey;
import com.bai.insightapps.utilities.DateUtils;
import com.bai.insightapps.utilities.FileOperation;
import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FilterBottomSheetView {

    public interface SetOnButtonClick {
        void setOnClearFilterClick();

        void setOnApplyFilterClick();
    }

    Calendar calendarFrom, calendarTo;
    Activity activity;
    SetOnButtonClick setOnButtonClick;
    LayoutFilterBottomSheetBinding binding;
    BottomSheetDialog bottomSheetDialog;
    int flagDate = 1;

    public FilterBottomSheetView(Activity activity, SetOnButtonClick setOnButtonClick) {
        this.activity = activity;
        this.setOnButtonClick = setOnButtonClick;
    }

    ArrayList<Info> listDaily, listWeekly, listMonthly;
    ArrayList<Info> listCommand;

    ArrayList<Info> listDailyFilter = new ArrayList<>(), listWeeklyFilter = new ArrayList<>(), listMonthlyFilter = new ArrayList<>();
    ArrayList<Info> listCommandFilter = new ArrayList<>();

    public void getArrayListFilter() {
        listCommand = FileOperation.getListCommand();
        listDaily = FileOperation.getListDailyInsight();
        listMonthly = FileOperation.getListMonthlyInsight();
        listWeekly = FileOperation.getListWeeklyInsight();
    }

    int flag = 0;

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public void showBottomSheetSort() {
        binding
                = DataBindingUtil.inflate(LayoutInflater.from(activity)
                , R.layout.layout_filter_bottom_sheet, null, false);
        bottomSheetDialog = new BottomSheetDialog(activity, R.style.BottomSheetDialog);
        bottomSheetDialog.setContentView(binding.getRoot());
        bottomSheetDialog.show();


        ArrayAdapter adapterTopic = new ArrayAdapter<String>(activity,
                android.R.layout.simple_spinner_item, FileOperation.getListTopic());
        adapterTopic.setDropDownViewResource(R.layout.spinner_dropdown_item);
        binding.spTopic.setAdapter(adapterTopic);
        ArrayAdapter adapterReadStus = ArrayAdapter.createFromResource(activity,
                R.array.readStatusArray, R.layout.spinner_item);
        adapterReadStus.setDropDownViewResource(R.layout.spinner_dropdown_item);
        binding.spReadStatus.setAdapter(adapterReadStus);

        setClickOnButton();
        setDataInView();
    }

    private void setDataInView() {
        flagDate = ((MainActivity) activity).getDateFlag();
        if (flagDate == 1) {
            binding.rbAllDates.setChecked(true);
        } else {
            binding.rbCustomDate.setChecked(true);
        }

        searchWord = ((MainActivity) activity).getSelWord();
        binding.edtWord.setText(searchWord);

        dateFrom = ((MainActivity) activity).getDateFrom();
        dateTo = ((MainActivity) activity).getDateTo();
        if (dateFrom == null && dateTo == null) {
            calendarFrom = Calendar.getInstance();
            calendarTo = Calendar.getInstance();
            dateFrom = calendarFrom.getTime();
            dateTo = calendarTo.getTime();
        } else {
            calendarFrom.setTime(dateFrom);
            calendarTo.setTime(dateTo);
        }
        binding.tvStarDate.setText(DateUtils.getDateDDMMYYYY(dateFrom.getTime()));
        binding.tvToDate.setText(DateUtils.getDateDDMMYYYY(dateTo.getTime()));

        binding.spTopic.setSelection(((MainActivity) activity).getIntSelTopicPos());
        binding.spReadStatus.setSelection(((MainActivity) activity).getSelReadStatusPos());
    }


    private void setClickOnButton() {
        binding.tvApply.setOnClickListener(clickOnButton);
        binding.tvClear.setOnClickListener(clickOnButton);
        binding.rgDate.setOnCheckedChangeListener(checkedChangeListener);
        binding.tvStarDate.setOnClickListener(clickOnButton);
        binding.tvToDate.setOnClickListener(clickOnButton);
    }


    private View.OnClickListener clickOnButton = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tvClear:
                    binding.rbAllDates.setChecked(true);
                    binding.rbCustomDate.setChecked(false);
                    calendarFrom = Calendar.getInstance();
                    calendarTo = Calendar.getInstance();
                    binding.tvStarDate.setText(DateUtils.getDateDDMMYYYY(calendarFrom.getTimeInMillis()));
                    binding.tvToDate.setText(DateUtils.getDateDDMMYYYY(calendarTo.getTimeInMillis()));
                    dateFrom = calendarFrom.getTime();
                    dateTo = calendarTo.getTime();
                    binding.spReadStatus.setSelection(0);
                    binding.spTopic.setSelection(0);
                    binding.edtWord.setText("");
                    flagDate = 1;
                    intTopicPos = 0;
                    intReadStatusPos = 0;
                    searchWord = "";
                    bottomSheetDialog.dismiss();
                    clearData();
                    sendSortMessage();
                    ((MainActivity) activity).setFilterSelectedData(dateFrom, dateTo, flagDate, intTopicPos, intReadStatusPos, searchWord);
                    ((MainActivity) activity).changeFilterIconColor(1);
                    //setOnButtonClick.setOnClearFilterClick();
                    break;
                case R.id.tvApply:
                    bottomSheetDialog.dismiss();
                    filterArrayList();
                    sendSortMessage();
                    ((MainActivity) activity).setFilterSelectedData(dateFrom, dateTo, flagDate, intTopicPos, intReadStatusPos, searchWord);
                    ((MainActivity) activity).changeFilterIconColor(2);
                    //setOnButtonClick.setOnApplyFilterClick();
                    break;
                case R.id.tvStarDate:
                    openFromDateCalender();
                    break;
                case R.id.tvToDate:
                    openToDateCalender();
                    break;
            }
        }
    };

    private void clearData() {
        listCommandFilter = FileOperation.getListCommand();
        listDailyFilter = FileOperation.getListDailyInsight();
        listMonthlyFilter = FileOperation.getListMonthlyInsight();
        listWeeklyFilter = FileOperation.getListWeeklyInsight();
    }

    RadioGroup.OnCheckedChangeListener checkedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int radioButtonId) {
            switch (radioButtonId) {
                case R.id.rbAllDates:
                    flagDate = 1;
                    showHideDateView(true);
                    break;
                case R.id.rbCustomDate:
                    flagDate = 2;
                    showHideDateView(false);
                    break;
            }
        }
    };

    private void showHideDateView(boolean isHideDateViews) {
        if (isHideDateViews) {
            binding.llFilterDate.setVisibility(View.GONE);
        } else {
            binding.llFilterDate.setVisibility(View.VISIBLE);
        }
    }

    Date dateFrom, dateTo;

    private void openFromDateCalender() {

        DateUtils.openDatePickerDialog(activity, calendarFrom, new OnDateChange() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendarFrom.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
                calendarTo.set(year, monthOfYear, dayOfMonth, 11, 59, 59);
                dateFrom = calendarFrom.getTime();
                dateTo = calendarTo.getTime();

                binding.tvStarDate.setText(DateUtils.getDateDDMMYYYY(calendarFrom.getTimeInMillis()));
                openToDateCalender();
            }

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            }
        }).show();
    }

    private void openToDateCalender() {
        DatePickerDialog datePickerDialog = DateUtils.openDatePickerDialog(activity, calendarTo, new OnDateChange() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendarTo.set(year, monthOfYear, dayOfMonth, 11, 59, 59);
                dateTo = calendarTo.getTime();
                binding.tvToDate.setText(DateUtils.getDateDDMMYYYY(calendarTo.getTimeInMillis()));
            }

            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            }
        });
        datePickerDialog.getDatePicker().setMinDate(calendarTo.getTimeInMillis());
        datePickerDialog.show();
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                binding.tvToDate.setText(DateUtils.getDateDDMMYYYY(calendarTo.getTimeInMillis()));
            }
        });
    }

    String topic;
    boolean readStatus;
    int intTopicPos, intReadStatusPos;
    String searchWord = "";

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void filterArrayList() {
        intTopicPos = binding.spTopic.getSelectedItemPosition();
        intReadStatusPos = binding.spReadStatus.getSelectedItemPosition();
        searchWord = binding.edtWord.getText().toString();
        if (flagDate == 1 && intTopicPos == 0 && searchWord.isEmpty() && intReadStatusPos == 0) {
            clearData();
        } else {
            filterArrayListCommand();
            filterArrayListInsight();
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void filterArrayListCommand() {

        if (intTopicPos != 0) {
            topic = (String) binding.spTopic.getSelectedItem();

        }
        if (intReadStatusPos != 0) {
            if (intReadStatusPos == 2) {
                readStatus = false;
            } else if (intReadStatusPos == 3) {
                readStatus = true;
            }
        }
        if (flagDate != 1) {
            Predicate<Info> byDate = info -> info.getMsgDate().after(dateFrom)
                    && info.getMsgDate().before(dateTo);
            listCommandFilter = (ArrayList<Info>) listCommand.stream().filter(byDate).collect(Collectors.toList());
        } else {
            listCommandFilter = listCommand;
        }

        if (intTopicPos != 0) {
            Predicate<Info> byTopic = info -> info.getTopic().contains(topic);
            listCommandFilter = (ArrayList<Info>) listCommandFilter.stream().filter(byTopic).collect(Collectors.toList());
        }
        if (!binding.edtWord.getText().toString().isEmpty()) {
            Predicate<Info> byWord = info -> info.getMessage().toLowerCase().contains(binding.edtWord.getText().toString().toLowerCase());
            listCommandFilter = (ArrayList<Info>) listCommandFilter.stream().filter(byWord).collect(Collectors.toList());
        }

        if (intReadStatusPos != 0) {
            Predicate<Info> byReadStatus = info -> info.getIsRead().equals(readStatus);
            listCommandFilter = (ArrayList<Info>) listCommandFilter.stream().filter(byReadStatus).collect(Collectors.toList());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void filterArrayListInsight() {

        if (intTopicPos != 0) {
            topic = (String) binding.spTopic.getSelectedItem();

        }
        if (intReadStatusPos != 0) {
            if (intReadStatusPos == 2) {
                readStatus = false;
            } else if (intReadStatusPos == 3) {
                readStatus = true;
            }
        }
        if (flagDate != 1) {
            Predicate<Info> byDate = info -> info.getMsgDate().after(dateFrom)
                    && info.getMsgDate().before(dateTo);
            listDailyFilter = (ArrayList<Info>) listDaily.stream().filter(byDate).collect(Collectors.toList());
            listWeeklyFilter = (ArrayList<Info>) listWeekly.stream().filter(byDate).collect(Collectors.toList());
            listMonthlyFilter = (ArrayList<Info>) listMonthly.stream().filter(byDate).collect(Collectors.toList());
        } else {
            listDailyFilter = listDaily;
            listWeeklyFilter = listWeekly;
            listMonthlyFilter = listMonthly;
        }

        if (intTopicPos != 0) {
            Predicate<Info> byTopic = info -> info.getTopic().contains(topic);
            listDailyFilter = (ArrayList<Info>) listDailyFilter.stream().filter(byTopic).collect(Collectors.toList());
            listWeeklyFilter = (ArrayList<Info>) listWeeklyFilter.stream().filter(byTopic).collect(Collectors.toList());
            listMonthlyFilter = (ArrayList<Info>) listMonthlyFilter.stream().filter(byTopic).collect(Collectors.toList());
        }
        if (!binding.edtWord.getText().toString().isEmpty()) {
            Predicate<Info> byWord = info -> info.getMessage().toLowerCase().contains(binding.edtWord.getText().toString().toLowerCase());
            listDailyFilter = (ArrayList<Info>) listDailyFilter.stream().filter(byWord).collect(Collectors.toList());
            listWeeklyFilter = (ArrayList<Info>) listWeeklyFilter.stream().filter(byWord).collect(Collectors.toList());
            listMonthlyFilter = (ArrayList<Info>) listMonthlyFilter.stream().filter(byWord).collect(Collectors.toList());
        }

        if (intReadStatusPos != 0) {
            Predicate<Info> byReadStatus = info -> info.getIsRead().equals(readStatus);
            listDailyFilter = (ArrayList<Info>) listDailyFilter.stream().filter(byReadStatus).collect(Collectors.toList());
            listWeeklyFilter = (ArrayList<Info>) listWeeklyFilter.stream().filter(byReadStatus).collect(Collectors.toList());
            listMonthlyFilter = (ArrayList<Info>) listMonthlyFilter.stream().filter(byReadStatus).collect(Collectors.toList());
        }
    }

    private void sendSortMessage() {
        Log.d("sender", "Sort Broadcasting message");
        Intent intent = new Intent(ConstantBundleKey.ACTION_SORT_COMMAND);
        // You can also include some extra data.
        intent.putExtra(ConstantBundleKey.COMMAND_DATA, listCommandFilter);
        intent.putExtra(ConstantBundleKey.DAILY_DATA, listDailyFilter);
        intent.putExtra(ConstantBundleKey.WEEKLY_DATA, listWeeklyFilter);
        intent.putExtra(ConstantBundleKey.MONTHLY_DATA, listMonthlyFilter);
        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
    }

}
