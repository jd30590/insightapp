package com.bai.insightapps.bottomsheet;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.telecom.ConnectionService;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.annotation.RequiresApi;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bai.insightapps.R;
import com.bai.insightapps.activities.MainActivity;
import com.bai.insightapps.activities.SortActivity;
import com.bai.insightapps.databinding.LayoutFilterBottomSheetBinding;
import com.bai.insightapps.databinding.LoyoutBottomSortBinding;
import com.bai.insightapps.model.Info;
import com.bai.insightapps.model.data.DataDetail;
import com.bai.insightapps.utilities.ConstantBundleKey;
import com.bai.insightapps.utilities.FileOperation;
import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SortBottomSheetView {

    public interface SetOnButtonClick {
        void setOnClearButtonClick();

        void setOnApplyButtonClick();
    }

    Activity activity;
    SetOnButtonClick setOnButtonClick;
    LoyoutBottomSortBinding binding;
    BottomSheetDialog bottomSheetDialog;
    boolean isSwitch1On = false, isSwitch2On = false, isSwitch3On = false;
    int sp1SelItem = 0, sp2SelItem = 0, sp3SelItem = 0;
    ArrayList<Info> listDaily, listWeekly, listMonthly;
    ArrayList<Info> listCommand;
    int flag = 0;

    public SortBottomSheetView(Activity activity, SetOnButtonClick setOnButtonClick) {
        this.activity = activity;
        this.setOnButtonClick = setOnButtonClick;
        binding
                = DataBindingUtil.inflate(LayoutInflater.from(activity)
                , R.layout.loyout_bottom_sort, null, false);
        bottomSheetDialog = new BottomSheetDialog(activity, R.style.BottomSheetDialog);
        bottomSheetDialog.setContentView(binding.getRoot());
        binding.spinner1.setSelection(0);
        binding.spinner2.setSelection(1);
        binding.spinner3.setSelection(2);


        binding.switch1.setOn(true);
        binding.switch2.setOn(true);
        binding.switch3.setOn(true);

        setBackgroundColorSwitch(binding.switch1, binding.switch2, binding.switch3);

        isSwitch1On = true;
        isSwitch2On = true;
        isSwitch3On = true;
        setTypeFaceSwitch();
        setSwitchEvent();
        setClickOnButton();
        initSpinner();
    }

    private void setBackgroundColorSwitch(LabeledSwitch switch1, LabeledSwitch switch2, LabeledSwitch switch3) {
        setSwitchColor((LabeledSwitch) switch1
                , activity.getResources().getColor(R.color.color_item_selected),
                activity.getResources().getColor(R.color.white));
        setSwitchColor((LabeledSwitch) switch2
                , activity.getResources().getColor(R.color.color_item_selected),
                activity.getResources().getColor(R.color.white));
        setSwitchColor((LabeledSwitch) switch3
                , activity.getResources().getColor(R.color.color_item_selected),
                activity.getResources().getColor(R.color.white));
    }

    public void getArrayList() {
        listCommand = FileOperation.getListCommandFilter();
        listDaily = FileOperation.getListDailyInsightFilter();
        listMonthly = FileOperation.getListMonthlyInsightFilter();
        listWeekly = FileOperation.getListWeeklyInsightFilter();
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public void showBottomSheetSort() {
        bottomSheetDialog.show();
    }


    private void setClickOnButton() {
        binding.tvApply.setOnClickListener(clickOnButton);
        binding.tvClear.setOnClickListener(clickOnButton);
    }


    private View.OnClickListener clickOnButton = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onClick(View view) {
            bottomSheetDialog.dismiss();
            switch (view.getId()) {
                case R.id.tvClear:
                    applyClearCommandSort(true);
                    //setOnButtonClick.setOnClearButtonClick();
                    break;
                case R.id.tvApply:
                    applyClearCommandSort(false);
                    // setOnButtonClick.setOnApplyButtonClick();
                    break;
            }

        }
    };

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void applyClearCommandSort(boolean isClearSort) {
        if (isClearSort) {
            binding.spinner1.setSelection(0);
            binding.spinner2.setSelection(1);
            binding.spinner3.setSelection(2);
            binding.switch1.setOn(true);
            binding.switch2.setOn(true);
            binding.switch3.setOn(true);
            isSwitch1On = true;
            isSwitch2On = true;
            isSwitch3On = true;
            setBackgroundColorSwitch(binding.switch1, binding.switch2, binding.switch3);
        }
        sortCommand();
        sortInsightData();
        sendSortMessage();
    }

    private void sendSortMessage() {
        Log.d("sender", "Sort Broadcasting message");
        Intent intent = new Intent(ConstantBundleKey.ACTION_SORT_COMMAND);
        // You can also include some extra data.
        intent.putExtra(ConstantBundleKey.COMMAND_DATA, listCommand);
        intent.putExtra(ConstantBundleKey.DAILY_DATA, listDaily);
        intent.putExtra(ConstantBundleKey.WEEKLY_DATA, listWeekly);
        intent.putExtra(ConstantBundleKey.MONTHLY_DATA, listMonthly);
        LocalBroadcastManager.getInstance(activity).sendBroadcast(intent);
    }

    //Date
    //Topic
    //Read status

    //Sort Insight Data
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void sortInsightData() {

        if (sp1SelItem == 0) {
            Collections.sort(listDaily, dateCmdSortAsc);
            Collections.sort(listWeekly, dateCmdSortAsc);
            Collections.sort(listMonthly, dateCmdSortAsc);
        }
        if (sp1SelItem == 1) {
            Collections.sort(listDaily, topicCmdSortAsc);
            Collections.sort(listWeekly, topicCmdSortAsc);
            Collections.sort(listMonthly, topicCmdSortAsc);
        }
        if (sp1SelItem == 2) {
            Collections.sort(listDaily, readStatusCmdAsc);
            Collections.sort(listWeekly, readStatusCmdAsc);
            Collections.sort(listMonthly, readStatusCmdAsc);
        }

        if (sp2SelItem == 0) {
            Collections.sort(listDaily, dateCmdSortAsc);
            Collections.sort(listWeekly, dateCmdSortAsc);
            Collections.sort(listMonthly, dateCmdSortAsc);
        }
        if (sp2SelItem == 1) {
            Collections.sort(listDaily, topicCmdSortAsc);
            Collections.sort(listWeekly, topicCmdSortAsc);
            Collections.sort(listMonthly, topicCmdSortAsc);
        }
        if (sp2SelItem == 2) {
            Collections.sort(listDaily, readStatusCmdAsc);
            Collections.sort(listWeekly, readStatusCmdAsc);
            Collections.sort(listMonthly, readStatusCmdAsc);
        }


        if (sp3SelItem == 0) {
            Collections.sort(listDaily, dateCmdSortAsc);
            Collections.sort(listWeekly, dateCmdSortAsc);
            Collections.sort(listMonthly, dateCmdSortAsc);
        }
        if (sp3SelItem == 1) {
            Collections.sort(listDaily, topicCmdSortAsc);
            Collections.sort(listWeekly, topicCmdSortAsc);
            Collections.sort(listMonthly, topicCmdSortAsc);
        }
        if (sp3SelItem == 2) {
            Collections.sort(listDaily, readStatusCmdAsc);
            Collections.sort(listWeekly, readStatusCmdAsc);
            Collections.sort(listMonthly, readStatusCmdAsc);
        }


        if (!isSwitch1On) {
            Collections.sort(listDaily, dateCmdSortDesc);
            Collections.sort(listWeekly, dateCmdSortDesc);
            Collections.sort(listMonthly, dateCmdSortDesc);
        }

        if (!isSwitch2On) {
            Collections.sort(listDaily, topicCmdSortDesc);
            Collections.sort(listWeekly, topicCmdSortDesc);
            Collections.sort(listMonthly, topicCmdSortDesc);
        }

        if (!isSwitch3On) {
            Collections.sort(listDaily, readStatusCmdDesc);
            Collections.sort(listWeekly, readStatusCmdDesc);
            Collections.sort(listMonthly, readStatusCmdDesc);
        }
    }

    //Command data sort
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void sortCommand() {

        if (sp1SelItem == 0) {
            Collections.sort(listCommand, dateCmdSortAsc);
        }
        if (sp1SelItem == 1) {
            Collections.sort(listCommand, topicCmdSortAsc);
        }
        if (sp1SelItem == 2) {
            Collections.sort(listCommand, readStatusCmdAsc);
        }

        if (sp2SelItem == 0) {
            Collections.sort(listCommand, dateCmdSortAsc);
        }
        if (sp2SelItem == 1) {
            Collections.sort(listCommand, topicCmdSortAsc);
        }
        if (sp2SelItem == 2) {
            Collections.sort(listCommand, readStatusCmdAsc);
        }

        if (sp3SelItem == 0) {
            Collections.sort(listCommand, dateCmdSortAsc);
        }
        if (sp3SelItem == 1) {
            Collections.sort(listCommand, topicCmdSortAsc);
        }
        if (sp3SelItem == 2) {
            Collections.sort(listCommand, readStatusCmdAsc);
        }

        if (!isSwitch1On) {
            Collections.sort(listCommand, dateCmdSortDesc);
        }

        if (!isSwitch2On) {
            Collections.sort(listCommand, topicCmdSortDesc);
        }

        if (!isSwitch3On) {
            Collections.sort(listCommand, readStatusCmdDesc);
        }
    }


    Comparator<Info> dateCmdSortAsc = new Comparator<Info>() {
        @Override
        public int compare(Info info1, Info info2) {
            return Long.compare(info1.getMsgDate().getTime(), info2.getMsgDate().getTime());
        }
    };
    Comparator<Info> topicCmdSortAsc = new Comparator<Info>() {
        @Override
        public int compare(Info info1, Info info2) {
            return info1.getTopic().compareTo(info2.getTopic());
        }
    };

    Comparator<Info> readStatusCmdAsc = new Comparator<Info>() {
        @Override
        public int compare(Info info1, Info info2) {
            return info1.getIsRead().compareTo(info1.getIsRead());
        }
    };

    Comparator<Info> dateCmdSortDesc = new Comparator<Info>() {
        @Override
        public int compare(Info info1, Info info2) {
            return Long.compare(info2.getMsgDate().getTime(), info1.getMsgDate().getTime());
        }
    };
    Comparator<Info> topicCmdSortDesc = new Comparator<Info>() {
        @Override
        public int compare(Info info1, Info info2) {
            return info2.getTopic().compareTo(info1.getTopic());
        }
    };

    Comparator<Info> readStatusCmdDesc = new Comparator<Info>() {
        @Override
        public int compare(Info info1, Info info2) {
            return info2.getIsRead().compareTo(info1.getIsRead());
        }
    };


    private void setTypeFaceSwitch() {
        Typeface interExtraBold = ResourcesCompat.getFont(activity, R.font.inter_extrabold);
        binding.switch1.setTypeface(interExtraBold);
        binding.switch2.setTypeface(interExtraBold);
        binding.switch3.setTypeface(interExtraBold);
    }

    private void setSwitchEvent() {

        binding.switch1.setOnToggledListener(toggledListener);
        binding.switch2.setOnToggledListener(toggledListener);
        binding.switch3.setOnToggledListener(toggledListener);
    }

    OnToggledListener toggledListener = new OnToggledListener() {
        @Override
        public void onSwitched(ToggleableView toggleableView, boolean isOn) {
            if (isOn) {
                setSwitchColor((LabeledSwitch) toggleableView
                        , activity.getResources().getColor(R.color.color_item_selected),
                        activity.getResources().getColor(R.color.white));
            } else {
                setSwitchColor((LabeledSwitch) toggleableView
                        , activity.getResources().getColor(R.color.white),
                        activity.getResources().getColor(R.color.color_off_switch));
            }
            switch (toggleableView.getId()) {
                case R.id.switch1:
                    isSwitch1On = toggleableView.isOn();
                    break;
                case R.id.switch2:
                    isSwitch2On = toggleableView.isOn();
                    break;
                case R.id.switch3:
                    isSwitch3On = toggleableView.isOn();
                    break;
            }
        }
    };

    private void setSwitchColor(LabeledSwitch lblSwitch, int colorOn, int colorOff) {
        lblSwitch.setColorOn(colorOn);
        lblSwitch.setColorOff(colorOff);
    }

    private void initSpinner() {

        ArrayList<String> sortLevel1 = new ArrayList<>();

        sortLevel1.add(activity.getResources().getString(R.string.itemDate));
        sortLevel1.add(activity.getResources().getString(R.string.hintTopic));
        sortLevel1.add(activity.getResources().getString(R.string.hintReadStatus));

        binding.spinner1.setItem(sortLevel1);
        binding.spinner2.setItem(sortLevel1);
        binding.spinner3.setItem(sortLevel1);
        binding.spinner1.setSelection(0);
        binding.spinner2.setSelection(1);
        binding.spinner3.setSelection(2);

        binding.spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                //Toast.makeText(SortActivity.this, sortLevel1.get(position), Toast.LENGTH_SHORT).show();
                sp1SelItem = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        binding.spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                // Toast.makeText(SortActivity.this, sortLevel1.get(position), Toast.LENGTH_SHORT).show();
                sp2SelItem = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        binding.spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                // Toast.makeText(SortActivity.this, sortLevel1.get(position), Toast.LENGTH_SHORT).show();
                sp3SelItem = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

}
