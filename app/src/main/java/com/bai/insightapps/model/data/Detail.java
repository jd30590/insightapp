
package com.bai.insightapps.model.data;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail implements Serializable {

    @SerializedName("MessageId")
    @Expose
    private Integer messageId;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("DataDetail")
    @Expose
    private List<DataDetail> dataDetail = null;

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<DataDetail> getDataDetail() {
        return dataDetail;
    }

    public void setDataDetail(List<DataDetail> dataDetail) {
        this.dataDetail = dataDetail;
    }

}
