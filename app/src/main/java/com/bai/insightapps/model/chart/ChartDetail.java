
package com.bai.insightapps.model.chart;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChartDetail implements Serializable {

    @SerializedName("ChartId")
    @Expose
    private Integer chartId;
    @SerializedName("MessageId")
    @Expose
    private Integer messageId;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("ChartType")
    @Expose
    private String chartType;
    @SerializedName("ChartData")
    @Expose
    private List<ChartDatum> chartData = null;

    public Integer getChartId() {
        return chartId;
    }

    public void setChartId(Integer chartId) {
        this.chartId = chartId;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChartType() {
        return chartType;
    }

    public void setChartType(String chartType) {
        this.chartType = chartType;
    }

    public List<ChartDatum> getChartData() {
        return chartData;
    }

    public void setChartData(List<ChartDatum> chartData) {
        this.chartData = chartData;
    }

}
