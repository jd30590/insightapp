
package com.bai.insightapps.model.chart;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail implements Serializable {

    @SerializedName("ChartDetail")
    @Expose
    private List<ChartDetail> chartDetail = null;

    public List<ChartDetail> getChartDetail() {
        return chartDetail;
    }

    public void setChartDetail(List<ChartDetail> chartDetail) {
        this.chartDetail = chartDetail;
    }

}
