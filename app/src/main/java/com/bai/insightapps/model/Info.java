package com.bai.insightapps.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class Info implements Serializable {
    @SerializedName("MessageId")
    @Expose
    private Integer messageId;
    @SerializedName("MessageTipe")
    @Expose
    private String messageTipe;
    @SerializedName("BroadcastMode")
    @Expose
    private String broadcastMode;
    @SerializedName("Topic")
    @Expose
    private String topic;
    @SerializedName("Period")
    @Expose
    private String period;
    @SerializedName("MsgTime")
    @Expose
    private String msgTime;
    @SerializedName("Sender")
    @Expose
    private String sender;
    @SerializedName("IsNeedResponse")
    @Expose
    private Boolean isNeedResponse;
    @SerializedName("IsRead")
    @Expose
    private Boolean isRead;
    @SerializedName("Message")
    @Expose
    private String message;

    Date msgDate = new Date();

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public String getMessageTipe() {
        return messageTipe;
    }

    public void setMessageTipe(String messageTipe) {
        this.messageTipe = messageTipe;
    }

    public String getBroadcastMode() {
        return broadcastMode;
    }

    public void setBroadcastMode(String broadcastMode) {
        this.broadcastMode = broadcastMode;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public Boolean getIsNeedResponse() {
        return isNeedResponse;
    }

    public void setIsNeedResponse(Boolean isNeedResponse) {
        this.isNeedResponse = isNeedResponse;
    }

    public Boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getMsgDate() {
        return msgDate;
    }

    public void setMsgDate(Date msgDate) {
        this.msgDate = msgDate;
    }

}
