
package com.bai.insightapps.model.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataDetail implements Serializable {

    @SerializedName(alternate = "Categori", value = "Category")
    @Expose
    private String categori;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Value")
    @Expose
    private String value;

    double valueTemp = 0;

    public String getCategori() {
        return categori;
    }

    public void setCategori(String categori) {
        this.categori = categori;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {

        this.value = value;
    }

    public double getValueTemp() {
        return valueTemp;
    }

    public void setValueTemp(double valueTemp) {
        this.valueTemp = valueTemp;
    }
}
