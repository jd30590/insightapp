package com.bai.insightapps.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListInfoModel implements Serializable {
    @SerializedName("ListInfo")
    @Expose
    private List<Info> listInfo = new ArrayList<>();

    public List<Info> getListInfo() {
        return listInfo;
    }

    public void setListInfo(List<Info> listInfo) {
        this.listInfo = listInfo;
    }
}
